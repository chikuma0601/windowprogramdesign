﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class CustomerPresentationModelTest
    {
        // test
        [TestMethod()]
        public void CustomerPresentationModelInitializeTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);

            Assert.AreEqual(false, presentation.IsAddButtonEnable);
        }

        // test
        [TestMethod()]
        public void InitializeButtonMapTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.InitializeButtonMap();
            List<string> namePrice = posModel.GetButtonText();
            Assert.AreEqual("Meal_A" + Environment.NewLine + "10", namePrice[0]);
        }

        // test
        [TestMethod()]
        public void GetMealButtonVisibleTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            List<bool> visible = presentation.GetMealButtonVisible();
            foreach (bool visibility in visible)
            {
                Assert.IsTrue(visibility);
            }
        }

        // test
        [TestMethod()]
        public void SetPageButtonEnableTest()
        {
            List<string> eventName = new List<string>();
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                eventName.Add(e.PropertyName);
            };
            presentation.SetPageButtonEnable();
            presentation.FlipNextPage();
            presentation.SetPageButtonEnable();
            Assert.AreEqual("IsNextPageEnable", eventName[0]);
            Assert.AreEqual("IsPreviousPageEnable", eventName[1]);
        }

        // test
        [TestMethod()]
        public void GetPriceTextTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();

            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            
            Assert.AreEqual("Price: 0$", presentation.GetPriceText());
        }

        // test
        [TestMethod()]
        public void AddMealToOrderTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            posModel.SetMealButtonMap();
            presentation.SetSelectedMeal(0);
         
            Assert.AreEqual(null, presentation.AddMealToOrder());
        }

        // test
        [TestMethod()]
        public void SetSelectedMealTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.SetSelectedMeal(0);
            Assert.AreEqual(-1, posModel.SelectedMealMapIndex);
        }

        // test
        [TestMethod()]
        public void FlipNextPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.FlipNextPage();
            Assert.AreEqual(2, posModel.Page);
        }

        // test
        [TestMethod()]
        public void FlipPreviousPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.FlipPreviousPage();
            Assert.AreEqual(1, posModel.Page);
        }

        // test
        [TestMethod()]
        public void ResetSelectedMealTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.ResetSelectedMeal();

            Assert.AreEqual(-1, posModel.SelectedMealMapIndex);
        }

        // test
        [TestMethod()]
        public void DeleteOrderTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.InitializeButtonMap();
            presentation.SetSelectedMeal(0);
            presentation.AddMealToOrder();
            try
            {
                presentation.DeleteOrder(0);
            } catch (Exception e)
            {
                Assert.IsNotNull(e);
            }
        }

        // test
        [TestMethod()]
        public void RefreshAddButtonEnableTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);

            presentation.RefreshAddButtonEnable();

            Assert.IsFalse(presentation.IsAddButtonEnable);
        }

        // test
        [TestMethod()]
        public void GetMealImageAbsolutePathTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.InitializeButtonMap();

            Assert.AreEqual(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + "\\Resources\\1.PNG", presentation.GetMealImageAbsolutePath()[0]);
        }

        // test
        [TestMethod()]
        public void GetDescriptionTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.InitializeButtonMap();

            Assert.AreEqual("", presentation.GetDescription());
        }

        // test
        [TestMethod()]
        public void ResetPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.InitializeButtonMap();
            presentation.ResetPage();
            Assert.AreEqual("Page: 1/2", presentation.GetPageLabel);
        }

        // test
        [TestMethod()]
        public void ChangeTabIndexTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.ChangeTabIndex(1);
            Assert.AreEqual("Meal_P", posModel.Menu[0].Name);
        }

        // test
        [TestMethod()]
        public void UpdateMealInformationTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            CustomerPresentationModel presentation = new CustomerPresentationModel(posModel);
            presentation.UpdateMealInformation();
            Assert.AreEqual(0, 0);
        }
    }
}