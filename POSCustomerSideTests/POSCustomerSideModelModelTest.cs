﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class POSCustomerSideModelModelTest
    {
        // test
        [TestMethod()]
        public void POSCustomerSideModelTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);

            Assert.AreEqual("Meal_A", posModel.Menu[0].Name);
        }

        // test
        [TestMethod()]
        public void SetMealButtonMapTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.FlipNextPage();
            posModel.SetMealButtonMap();

            Assert.AreEqual(6, posModel.GetMealOnPage());
        }

        // test
        [TestMethod()]
        public void GetButtonTextTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            List<string> namePrice = posModel.GetButtonText();

            Assert.AreEqual("Meal_A" + Environment.NewLine + "10", namePrice[0]);
            Assert.AreEqual("Meal_I" + Environment.NewLine + "90", namePrice[8]);
        }

        // test
        [TestMethod()]
        public void GetMealImageNameTest()
        {

            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            List<string> imageName = posModel.GetMealImageName();

            Assert.AreEqual("1.PNG", imageName[0]);
            Assert.AreEqual("9.PNG", imageName[8]);
        }

        // test
        [TestMethod()]
        public void GetTotalPagesTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);

            Assert.AreEqual(2, posModel.GetTotalPages());
        }

        // test
        [TestMethod()]
        public void ResetPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.FlipNextPage();
            posModel.ResetPage();

            Assert.AreEqual(1, posModel.Page);
        }

        // test
        [TestMethod()]
        public void FlipNextPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.FlipNextPage();
            posModel.FlipNextPage();

            Assert.AreEqual(2, posModel.Page);
        }

        // test
        [TestMethod()]
        public void FlipPreviousPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.FlipPreviousPage();

            Assert.AreEqual(1, posModel.Page);
            posModel.FlipNextPage();
            posModel.FlipPreviousPage();
            Assert.AreEqual(1, posModel.Page);
        }

        // test
        [TestMethod()]
        public void AddToOrderTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(2);

            Assert.AreEqual("おいしいラーメンBです。", posModel.GetSelectedMealDescription());
        }

        // test
        [TestMethod()]
        public void GetAddedOrderTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            Assert.AreEqual(null, posModel.GetAddedOrder());
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(2);
            posModel.AddToOrder();

            Assert.AreEqual("Meal_B", posModel.GetAddedOrder()[0]);
            Assert.AreEqual("20", posModel.GetAddedOrder()[1]);
        }

        // test
        [TestMethod()]
        public void GetTotalPriceTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(1);
            for (int i = 0; i < 10; ++i)
                posModel.AddToOrder();
            posModel.ConvertButtonIndexToMealIndex(2);
            for (int i = 0; i < 10; ++i)
                posModel.AddToOrder();

            Assert.AreEqual(300.ToString(), posModel.GetTotalPrice());
        }

        // test
        [TestMethod()]
        public void GetMaxButtonOnPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            
            Assert.AreEqual(9, posModel.GetMaxButtonOnPage());
        }

        // test
        [TestMethod()]
        public void GetPageLabelTextTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);

            Assert.AreEqual("Page: 1/2", posModel.GetPageLabelText());
        }

        // test
        [TestMethod()]
        public void IsAtFirstPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);

            Assert.IsTrue(posModel.IsAtFirstPage());
            posModel.SetMealButtonMap();
            posModel.FlipNextPage();
            Assert.IsFalse(posModel.IsAtFirstPage());
        }

        // test
        [TestMethod()]
        public void IsAtLastPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            Assert.IsFalse(posModel.IsAtLastPage());
            posModel.SetMealButtonMap();
            posModel.FlipNextPage();
            Assert.IsTrue(posModel.IsAtLastPage());
        }

        // test
        [TestMethod()]
        public void ResetSelectedMealTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(1);
            posModel.AddToOrder();
            posModel.ResetSelectedMeal();
            Assert.AreEqual(null, posModel.GetSelectedMealDescription());
        }

        // test
        [TestMethod()]
        public void GetMealOnPageTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();

            Assert.AreEqual(9, posModel.GetMealOnPage());
        }

        // test
        [TestMethod()]
        public void DeleteOrderByIndexTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(1);
            posModel.AddToOrder();
            posModel.DeleteOrderByIndex(0);

            Assert.AreEqual(0, posModel.Order.Orders.Count);
        }

        // test
        [TestMethod()]
        public void ConvertButtonIndexToMealIndexTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(1);

            Assert.AreEqual(0, posModel.GetSelectedMealMapIndex());
        }

        // test
        [TestMethod()]
        public void GetSelectedMealDescriptionTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.SetMealButtonMap();
            posModel.ConvertButtonIndexToMealIndex(1);

            Assert.AreEqual(0, posModel.SelectedMealMapIndex);
        }

        // test
        [TestMethod()]
        public void ChangeCurrentCategoryTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.ChangeCurrentCategory(1);
            Assert.AreEqual("Meal_P", posModel.Menu[0].Name);
        }

        // test
        [TestMethod()]
        public void GetCategoryIndexTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            Assert.AreEqual(0, posModel.GetCategoryIndex("Ramen"));
            Assert.AreEqual(-1, posModel.GetCategoryIndex(":("));
        }

        // test
        [TestMethod()]
        public void UpdateMealInformationTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            posModel.UpdateMealInformation();
            Assert.AreEqual(0, 0);
        }

        // test
        [TestMethod()]
        public void MealsInFileTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            Assert.AreEqual("Meal_A", posModel.MealsInFile[0].Name);
        }

        // test
        [TestMethod()]
        public void MenuCategoryTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            Assert.AreEqual("Ramen", posModel.MenuCategory[0]);
        }
    }
}