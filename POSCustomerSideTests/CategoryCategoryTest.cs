﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class CategoryCategoryTest
    {
        // test
        [TestMethod()]
        public void CategoryTest()
        {
            Category category = new Category("testName");

            Assert.AreEqual("testName", category.CategoryName);
            category.CategoryName = "crappyJunk";
            Assert.AreEqual("crappyJunk", category.CategoryName);
        }
    }
}