﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class RestaurantPresentationModelTest
    {
        // constructor test
        [TestMethod()]
        public void RestaurantPresentationModelConstructorTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentationModel = new RestaurantPresentationModel(posModel);
            for (int i = 0; i < posModel.Menu.Count; ++i)
                Assert.AreEqual(posModel.Menu[i], presentationModel.Meals[i]);
        }

        // test
        [TestMethod()]
        public void SetInformationToSelectedTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentationModel = new RestaurantPresentationModel(posModel);
            presentationModel.SetSelectedMeal(0);
            presentationModel.SetInformationToSelected();
            Assert.AreEqual("Meal_A", presentationModel.MealName);
            Assert.AreEqual("10", presentationModel.MealPrice);
            Assert.AreEqual("1.PNG", presentationModel.MealImagePath);
            Assert.AreEqual("おいしいラーメンAです。", presentationModel.MealDescription);
        }

        // test
        [TestMethod()]
        public void SetCategoryInformationToSelectedTest()
        {
            List<string> eventName = new List<string>();
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentation = new RestaurantPresentationModel(posModel);
            presentation.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                eventName.Add(e.PropertyName);
            };
            presentation.SetCategoryInformationToSelected();
            Assert.AreEqual("CategoryName", eventName[0]);
        }

        // test
        [TestMethod()]
        public void SetSelectedMealTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentation = new RestaurantPresentationModel(posModel);

            presentation.SetSelectedMeal(0);
            Assert.AreEqual("Meal_A", presentation.GetSelectedMeal.Name);
        }

        // test
        [TestMethod()]
        public void SaveMealTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentation = new RestaurantPresentationModel(posModel);

            presentation.SetSelectedMeal(0);

            presentation.MealName = "Meal_B";
            presentation.MealPrice = "20";
            presentation.MealDescription = ":(";
            presentation.MealImagePath = "D:";

            presentation.SaveMeal();

            Assert.AreEqual("Meal_B", presentation.GetSelectedMeal.Name);
            Assert.AreEqual(20, presentation.GetSelectedMeal.Price);
            Assert.AreEqual(":(", presentation.GetSelectedMeal.Description);
            Assert.AreEqual("D:", presentation.GetSelectedMeal.ImageName);
        }

        // test
        [TestMethod()]
        public void SetSelectedCategoryTest()
        {
            POSCustomerSideModel posModel = new POSCustomerSideModel(true);
            RestaurantPresentationModel presentation = new RestaurantPresentationModel(posModel);

            presentation.SetSelectedCategory(0);
            Assert.AreEqual("Ramen", presentation.Categories[0]);
        }
    }
}