﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class StartUpPresentationModelTest
    {
        // test
        [TestMethod()]
        public void IsCustomerButtonEnableTest()
        {
            StartUpPresentationModel startUp = new StartUpPresentationModel();
            Assert.AreEqual(true, startUp.IsCustomerButtonEnable());
        }

        // test
        [TestMethod()]
        public void IsRestaurantButtonEnableTest()
        {
            StartUpPresentationModel startUp = new StartUpPresentationModel();
            Assert.AreEqual(true, startUp.IsRestaurantButtonEnable());
        }

        // test
        [TestMethod()]
        public void SetCustomerEnableTest()
        {
            StartUpPresentationModel startUp = new StartUpPresentationModel();
            startUp.SetCustomerEnable(false);
            Assert.AreEqual(false, startUp.IsCustomerButtonEnable());
        }

        // test
        [TestMethod()]
        public void SetRestaurantEnableTest()
        {
            StartUpPresentationModel startUp = new StartUpPresentationModel();
            startUp.SetRestaurantEnable(false);
            Assert.AreEqual(false, startUp.IsRestaurantButtonEnable());
        }
    }
}