﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class SingleOrderTest
    {
        // test
        [TestMethod()]
        public void SingleOrderConstructTest()
        {
            Meal meal = new Meal("A", 10, "B", "C", "D");
            SingleOrder order = new SingleOrder(meal);

            Assert.AreEqual("A", order.Name);
            Assert.AreEqual(10, order.Subtotal);
            Assert.AreEqual(1, order.Quantity);
            Assert.AreEqual(10, order.UnitPrice);
            Assert.AreEqual("D", order.Category);
        }

        // test
        [TestMethod()]
        public void SetProperty()
        {
            Meal meal = new Meal("A", 10, "B", "C", "D");
            SingleOrder order = new SingleOrder(meal);
            List<string> eventName = new List<string>();

            order.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                eventName.Add(e.PropertyName);
            };

            order.Name = "new Name";
            order.Quantity = 2;
            order.Category = "some new stuff";
            order.UnitPrice = 100;

            Assert.AreEqual("Name", eventName[0]);
            Assert.AreEqual("Quantity", eventName[1]);
            Assert.AreEqual("Subtotal", eventName[2]);
            Assert.AreEqual("Category", eventName[3]);
            Assert.AreEqual("UnitPrice", eventName[4]);
        }
    }
}