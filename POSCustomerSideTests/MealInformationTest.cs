﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class MealInformationTest
    {
        // test
        [TestMethod()]
        public void MealInformationConstructorTest()
        {
            MealInformation information = new MealInformation(true);
            Assert.AreEqual("Meal_A", information.GetMealName[0]);
            Assert.AreEqual(10, information.GetPrice[0]);
            Assert.AreEqual("1.PNG", information.GetImageName[0]);
            Assert.AreEqual("おいしいラーメンAです。", information.GetDescription[0]);
            Assert.AreEqual("Ramen", information.GetCategory[0]);
            Assert.AreEqual("AddOn", information.MenuCategory[2]);
        }
    }
}