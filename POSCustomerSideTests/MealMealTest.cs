﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class MealMealTest
    {
        // test
        [TestMethod()]
        public void MealTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            Assert.AreEqual(new Meal("A", 10, "C", "D", "E"), meal);
        }

        // test
        [TestMethod()]
        public void GetNamePriceTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            Assert.AreEqual("A" + Environment.NewLine + "10", meal.GetNamePrice());
        }

        // test
        [TestMethod()]
        public void GetCloneTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            Meal clonedMeal = meal.GetClone();
            Assert.AreEqual(true, meal.Equals(clonedMeal));
        }

        // test
        [TestMethod()]
        public void EqualsTest()
        {
            Meal meal_1 = new Meal("A", 10, "C", "D", "E");
            Meal meal_2 = new Meal("A", 10, "C", "D", "E");
            Assert.AreEqual(true, meal_1.Equals(meal_2));
        }

        // Testing all properties in meal
        [TestMethod()]
        public void PropertyTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            Assert.AreEqual("A", meal.Name);
            Assert.AreEqual(10, meal.Price);
            Assert.AreEqual("C", meal.ImageName);
            Assert.AreEqual("D", meal.Description);
            Assert.AreEqual("E", meal.Category);
        }

        // test
        [TestMethod()]
        public void PropertyChangeTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            meal.Name = "B";
            meal.Price = 11;
            meal.Description = "D";
            meal.ImageName = "E";
            meal.Category = "F";
            Assert.AreEqual("B", meal.Name);
            Assert.AreEqual(11, meal.Price);
            Assert.AreEqual("D", meal.Description);
            Assert.AreEqual("E", meal.ImageName);
            Assert.AreEqual("F", meal.Category);
        }

        // test
        [TestMethod()]
        public void PropertyChangeEventTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            List<string> eventName = new List<string>();

            meal.PropertyChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                eventName.Add(e.PropertyName);
            };

            meal.Name = "B";
            meal.Price = 11;
            meal.Description = "D";
            meal.ImageName = "E";
            meal.Category = "F";

            Assert.AreEqual("Name", eventName[0]);
            Assert.AreEqual("Price", eventName[1]);
            Assert.AreEqual("Description", eventName[2]);
            Assert.AreEqual("ImageName", eventName[3]);
            Assert.AreEqual("Category", eventName[4]);
        }

    }
}