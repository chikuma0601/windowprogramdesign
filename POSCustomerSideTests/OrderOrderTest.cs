﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using POSCustomerSide;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide.Tests
{
    [TestClass()]
    public class OrderOrderTest
    {
        // test
        [TestMethod()]
        public void AddOrderTest()
        {
            Meal meal = new Meal("A", 10, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(meal);

            Assert.AreEqual(meal.Name, order.Orders[0].Name);
        }

        // test
        [TestMethod()]
        public void UpdateTotalPriceTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Meal mealB = new Meal("B", 20, "C", "D", "E");
            Order order = new Order();
            for (int i = 0; i < 10; ++i)
                order.AddOrder(mealA);
            order.AddOrder(mealB);

            Assert.AreEqual(120, order.GetTotalPrice());
        }

        // test
        [TestMethod()]
        public void GetTotalPriceTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(mealA);
            Assert.AreEqual(10, order.GetTotalPrice());
        }

        // test
        [TestMethod()]
        public void IsEmptyTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Order order = new Order();
            Assert.IsTrue(order.IsEmpty());
            order.AddOrder(mealA);
            Assert.IsFalse(order.IsEmpty());
        }

        // test
        [TestMethod()]
        public void GetLastOrderTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(mealA);

            Assert.AreEqual("A", order.GetLastOrder().Name);
        }
        
        // test
        [TestMethod()]
        public void GetLastOrderNamePriceTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Meal mealB = new Meal("B", 20, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(mealA);
            order.AddOrder(mealB);

            Assert.AreEqual("B", order.GetLastOrderNamePrice()[0]);
            Assert.AreEqual("20", order.GetLastOrderNamePrice()[1]);
        }

        // test
        [TestMethod()]
        public void DeleteMealByIndexTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Meal mealB = new Meal("B", 20, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(mealA);
            order.AddOrder(mealB);

            order.DeleteMealByIndex(0);
            Assert.AreEqual(20, order.GetTotalPrice());
        }

        // test
        [TestMethod()]
        public void QuantityTest()
        {
            Meal mealA = new Meal("A", 10, "C", "D", "E");
            Meal mealB = new Meal("B", 20, "C", "D", "E");
            Order order = new Order();
            order.AddOrder(mealA);
            order.AddOrder(mealA);
            order.AddOrder(mealB);

            Assert.AreEqual(2, order.Quantity[0]);
            Assert.AreEqual(1, order.Quantity[1]);
        }
    }
}