﻿namespace POSCustomerSide
{
    partial class StartUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._startUpTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this._startCustomerProgramButton = new System.Windows.Forms.Button();
            this._startRestaurantProgramButton = new System.Windows.Forms.Button();
            this._exitButton = new System.Windows.Forms.Button();
            this._startUpTablePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // _startUpTablePanel
            // 
            this._startUpTablePanel.ColumnCount = 1;
            this._startUpTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._startUpTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._startUpTablePanel.Controls.Add(this._startCustomerProgramButton, 0, 0);
            this._startUpTablePanel.Controls.Add(this._startRestaurantProgramButton, 0, 1);
            this._startUpTablePanel.Controls.Add(this._exitButton, 0, 2);
            this._startUpTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._startUpTablePanel.Location = new System.Drawing.Point(0, 0);
            this._startUpTablePanel.Name = "_startUpTablePanel";
            this._startUpTablePanel.RowCount = 3;
            this._startUpTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._startUpTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._startUpTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._startUpTablePanel.Size = new System.Drawing.Size(529, 283);
            this._startUpTablePanel.TabIndex = 0;
            // 
            // _startCustomerProgramButton
            // 
            this._startCustomerProgramButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._startCustomerProgramButton.Location = new System.Drawing.Point(3, 3);
            this._startCustomerProgramButton.Name = "_startCustomerProgramButton";
            this._startCustomerProgramButton.Size = new System.Drawing.Size(523, 88);
            this._startCustomerProgramButton.TabIndex = 0;
            this._startCustomerProgramButton.Text = "Start the Customer Program (Frontend)";
            this._startCustomerProgramButton.UseVisualStyleBackColor = true;
            this._startCustomerProgramButton.Click += new System.EventHandler(this.ClickCustomerProgram);
            // 
            // _startRestaurantProgramButton
            // 
            this._startRestaurantProgramButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._startRestaurantProgramButton.Location = new System.Drawing.Point(3, 97);
            this._startRestaurantProgramButton.Name = "_startRestaurantProgramButton";
            this._startRestaurantProgramButton.Size = new System.Drawing.Size(523, 88);
            this._startRestaurantProgramButton.TabIndex = 1;
            this._startRestaurantProgramButton.Text = "Start the Restaurant Program (Backend)";
            this._startRestaurantProgramButton.UseVisualStyleBackColor = true;
            this._startRestaurantProgramButton.Click += new System.EventHandler(this.ClickRestaurantProgram);
            // 
            // _exitButton
            // 
            this._exitButton.Dock = System.Windows.Forms.DockStyle.Right;
            this._exitButton.Location = new System.Drawing.Point(362, 191);
            this._exitButton.Name = "_exitButton";
            this._exitButton.Size = new System.Drawing.Size(164, 89);
            this._exitButton.TabIndex = 2;
            this._exitButton.Text = "Exit";
            this._exitButton.UseVisualStyleBackColor = true;
            this._exitButton.Click += new System.EventHandler(this.ExitProgram);
            // 
            // StartUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 283);
            this.Controls.Add(this._startUpTablePanel);
            this.Name = "StartUpForm";
            this.Text = "StartUpForm";
            this._startUpTablePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _startUpTablePanel;
        private System.Windows.Forms.Button _startCustomerProgramButton;
        private System.Windows.Forms.Button _startRestaurantProgramButton;
        private System.Windows.Forms.Button _exitButton;
    }
}