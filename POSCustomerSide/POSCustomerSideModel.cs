﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class POSCustomerSideModel// : INotifyPropertyChanged
    {
        //public event PropertyChangedEventHandler PropertyChanged;

        public POSCustomerSideModel (bool isTest = false)
        {
            InitializeMealInformation(isTest);
            InitializeCurrentCategory();
            InitializeMenu();
        }

        // Initialize meal information
        private void InitializeMealInformation(bool isTest)
        {
            MealInformation information = new MealInformation(isTest);
            _mealName = information.GetMealName;
            _mealPrice = information.GetPrice;
            _imageName = information.GetImageName;
            _description = information.GetDescription;
            _category = information.GetCategory;
            ConvertToBindingList(information.MenuCategory);
            AddAllToMealInFile();
        }

        // Converts List<string> to BindingList<string> for category
        private void ConvertToBindingList(List<string> list)
        {
            foreach (string category in list)
            {
                _menuCategory.Add(category);
            }
        }

        // Create a list of meal accroding to file
        private void AddAllToMealInFile()
        {
            for (int i = 0; i < _mealName.Count(); ++i)
            {
                _mealsInFile.Add(new Meal(_mealName[i], _mealPrice[i], _imageName[i], _description[i], _category[i]));
            }
        }

        // Initialize current category to the first category in category file
        private void InitializeCurrentCategory()
        {
            _currentCategory = _category[0];
        }

        // Init the menu, only saves the meal with category equals to current category
        private void InitializeMenu()
        {
            _menu = new List<Meal>();
            for (int i = 0; i < _mealsInFile.Count(); ++i)
            {
                if (_mealsInFile[i].Category == CurrentCategory)
                    _menu.Add(_mealsInFile[i]);
            }
        }

        // Use the current page, category and the menu to setup the buttonNumber - Meal map
        public void SetMealButtonMap()
        {
            _buttonMeal = new List<Meal>();
            for (int i = 0 ; i < _maxButtonOnPage; ++i)
            {
                if (GetMenuIndex(i) >= _menu.Count)
                    break;
                _buttonMeal.Add(_menu[GetMenuIndex(i)]);
            }
        }

        // Get the index in menu by current page and button_index on this page
        private int GetMenuIndex(int index)
        {
            return (_page - 1) * _maxButtonOnPage + index;
        }

        // Form the text on the button accordingly
        public List<string> GetButtonText()
        {
            List<string> buttonText = new List<string>();
            foreach (Meal meal in _buttonMeal)
            {
                buttonText.Add(meal.GetNamePrice());
            }
            return buttonText;
        }

        // Get the list of showed meal's image names
        public List<string> GetMealImageName()
        {
            List<string> imageNames = new List<string>();
            foreach (Meal meal in _buttonMeal)
            {
                if (meal != null)
                    imageNames.Add(meal.ImageName);
            }
            return imageNames;
        }

        // Get the total Pages of the buttons
        public int GetTotalPages()
        {
            int totalPages = _menu.Count / _maxButtonOnPage;
            if ((double)_menu.Count / (double)_maxButtonOnPage > totalPages)
                totalPages++;
            return totalPages;
        }

        // Get the page you are currently on
        public int Page
        {
            get 
            {
                return _page;
            }
        }

        // Resets the page if tab Changed
        public void ResetPage()
        {
            _page = 1;
        }

        // filp to next page and update the button-meal map
        public void FlipNextPage()
        {
            if (_page < GetTotalPages())
            {
                _page++;
                SetMealButtonMap();
            }
        }

        // flip to the previous page and update the button-meal map
        public void FlipPreviousPage()
        {
            if (_page > 1)
            {
                _page--;
                SetMealButtonMap();
            }
        }

        // Adds a meal to order
        public void AddToOrder()
        {
            if (_selectedMealMapIndex != -1)
            {
                _order.AddOrder(_menu[_selectedMealMapIndex]);
            }
        }

        // pass the order added to form
        public List<string> GetAddedOrder()
        {
            if (!_order.IsEmpty() && _selectedMealMapIndex != -1)
            {
                return GetLastOrderNamePrice();
            }
            else
            {
                return null;
            }
        }

        // Get the name and price of the last ordered meal
        private List<string> GetLastOrderNamePrice()
        {
            return _order.GetLastOrderNamePrice();
        }

        // Get the total price of the order
        public string GetTotalPrice()
        {
            return _order.GetTotalPrice().ToString();
        }

        // Get the maximum meal button, which is 9 
        public int GetMaxButtonOnPage()
        {
            return _maxButtonOnPage;
        }

        // Get the page label text which look something like: 1/3
        public string GetPageLabelText()
        {  
            return _pageText[0] + Page.ToString() + _pageText[1] + GetTotalPages().ToString();
        }

        // See if is at first page
        public bool IsAtFirstPage()
        {
            if (Page == 1)
                return true;
            return false;
        }

        // See if is at last page
        public bool IsAtLastPage()
        {
            if (Page == GetTotalPages())
                return true;
            return false;
        }

        // Reset the selectedMealMapIndex to -1
        public void ResetSelectedMeal()
        {
            _selectedMealMapIndex = -1;
        }

        // Get the number of meals on the current page
        public int GetMealOnPage()
        {
            return _buttonMeal.Count;
        }

        // Delete the order by index
        public void DeleteOrderByIndex(int orderIndex)
        {
            _order.DeleteMealByIndex(orderIndex);
        }

        // Get the selected meal index, -1 = none selected
        public int SelectedMealMapIndex
        {
            get
            {
                return _selectedMealMapIndex;
            }
        }

        // Store the selected meal number, converts the button number to meal index automatically
        public void ConvertButtonIndexToMealIndex(int buttonIndex)
        {
            _selectedMealMapIndex = (_page - 1) * _maxButtonOnPage + buttonIndex - 1;
        }

        // Get the selected meal index in _menu
        public int GetSelectedMealMapIndex ()
        {
            return _selectedMealMapIndex;
        }

        // Get the description of the selected meal
        public string GetSelectedMealDescription()
        {
            if (_selectedMealMapIndex == -1)
                return null;
            return _menu[_selectedMealMapIndex].Description;
        }

        public Order Order
        {
            get
            {
                return _order;
            }
        }

        public BindingList<string> MenuCategory
        {
            get
            {
                return _menuCategory;
            }
        }

        public string CurrentCategory
        {
            get
            {
                return _currentCategory;
            }
        }

        // the current category changed
        public void ChangeCurrentCategory(int index)
        {
            _currentCategory = _menuCategory[index];
            InitializeMenu();
            SetMealButtonMap();
        }

        public BindingList<Meal> MealsInFile
        {
            get
            {
                return _mealsInFile;
            }
        }

        public List<Meal> Menu
        {
            get
            {
                return _menu;
            }
        }

        // Get the category index by name, returns -1 if didn't found
        public int GetCategoryIndex(string categoryName)
        {
            for (int i = 0; i < _menuCategory.Count; ++i)
            {
                if (categoryName == _menuCategory[i])
                    return i;
            }
            return -1;
        }

        // should update meal information then order IDK how to implement though
        public void UpdateMealInformation()
        {

        }

        private int _selectedMealMapIndex = -1;
        private List<Meal> _menu = new List<Meal>();// Contains all meal by currentCategory
        private List<Meal> _buttonMeal = new List<Meal>();// Contains all meal on current page
        private BindingList<string> _menuCategory = new BindingList<string>();// All types of categories in file
        private Order _order = new Order();
        private int _page = 1;
        private string _currentCategory;
        private string[] _mealName;// Contains all stuffs in file
        private int[] _mealPrice;
        private string[] _imageName;
        private string[] _description;
        private string[] _category;
        private BindingList<Meal> _mealsInFile = new BindingList<Meal>();
        private readonly int _maxButtonOnPage = 9;
        private readonly string[] _pageText = { "Page: ", "/" };
    }
}
