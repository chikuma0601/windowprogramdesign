﻿namespace POSCustomerSide
{
    partial class POSRestaurantSideForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._managerTabControl = new System.Windows.Forms.TabControl();
            this._mealManager = new System.Windows.Forms.TabPage();
            this._mealManagerTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._dataListTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._addDeleteTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._addMealButton = new System.Windows.Forms.Button();
            this._deleteMealButton = new System.Windows.Forms.Button();
            this._mealListBox = new System.Windows.Forms.ListBox();
            this._mealEditGroupBox = new System.Windows.Forms.GroupBox();
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._mealImageLabel = new System.Windows.Forms.Label();
            this._mealImagePathTextBox = new System.Windows.Forms.TextBox();
            this._browseButton = new System.Windows.Forms.Button();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._mealPriceLabel = new System.Windows.Forms.Label();
            this._mealPriceTextBox = new System.Windows.Forms.TextBox();
            this._dollarLabel = new System.Windows.Forms.Label();
            this._categoryComboBox = new System.Windows.Forms.ComboBox();
            this._categoryLabel = new System.Windows.Forms.Label();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._mealNameLabel = new System.Windows.Forms.Label();
            this._mealNameTextBox = new System.Windows.Forms.TextBox();
            this._descriptionLabel = new System.Windows.Forms.Label();
            this._mealDescriptionTextBox = new System.Windows.Forms.TextBox();
            this._mealSaveButton = new System.Windows.Forms.Button();
            this._categoryManager = new System.Windows.Forms.TabPage();
            this._categoryManageTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this._categoryTableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._categoryListBox = new System.Windows.Forms.ListBox();
            this._addDeleteTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._addCategoryButton = new System.Windows.Forms.Button();
            this._deleteCategoryButton = new System.Windows.Forms.Button();
            this._categoryGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._categoryNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._mealUsedCategoryList = new System.Windows.Forms.ListBox();
            this._categorySaveButton = new System.Windows.Forms.Button();
            this._categoryNameTextBox = new System.Windows.Forms.TextBox();
            this._managerTabControl.SuspendLayout();
            this._mealManager.SuspendLayout();
            this._mealManagerTableLayout.SuspendLayout();
            this._dataListTableLayout.SuspendLayout();
            this._addDeleteTableLayout.SuspendLayout();
            this._mealEditGroupBox.SuspendLayout();
            this._tableLayoutPanel1.SuspendLayout();
            this._tableLayoutPanel4.SuspendLayout();
            this._tableLayoutPanel3.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._categoryManager.SuspendLayout();
            this._categoryManageTableLayout.SuspendLayout();
            this._categoryTableLayoutPanel1.SuspendLayout();
            this._addDeleteTableLayoutPanel.SuspendLayout();
            this._categoryGroupBox.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // _managerTabControl
            // 
            this._managerTabControl.Controls.Add(this._mealManager);
            this._managerTabControl.Controls.Add(this._categoryManager);
            this._managerTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._managerTabControl.Location = new System.Drawing.Point(0, 0);
            this._managerTabControl.Name = "_managerTabControl";
            this._managerTabControl.SelectedIndex = 0;
            this._managerTabControl.Size = new System.Drawing.Size(951, 481);
            this._managerTabControl.TabIndex = 0;
            // 
            // _mealManager
            // 
            this._mealManager.Controls.Add(this._mealManagerTableLayout);
            this._mealManager.Location = new System.Drawing.Point(4, 22);
            this._mealManager.Name = "_mealManager";
            this._mealManager.Padding = new System.Windows.Forms.Padding(3);
            this._mealManager.Size = new System.Drawing.Size(943, 455);
            this._mealManager.TabIndex = 0;
            this._mealManager.Text = "Meal Manager";
            this._mealManager.UseVisualStyleBackColor = true;
            // 
            // _mealManagerTableLayout
            // 
            this._mealManagerTableLayout.ColumnCount = 2;
            this._mealManagerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this._mealManagerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this._mealManagerTableLayout.Controls.Add(this._dataListTableLayout, 0, 0);
            this._mealManagerTableLayout.Controls.Add(this._mealEditGroupBox, 1, 0);
            this._mealManagerTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealManagerTableLayout.Location = new System.Drawing.Point(3, 3);
            this._mealManagerTableLayout.Name = "_mealManagerTableLayout";
            this._mealManagerTableLayout.RowCount = 1;
            this._mealManagerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mealManagerTableLayout.Size = new System.Drawing.Size(937, 449);
            this._mealManagerTableLayout.TabIndex = 0;
            // 
            // _dataListTableLayout
            // 
            this._dataListTableLayout.ColumnCount = 1;
            this._dataListTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._dataListTableLayout.Controls.Add(this._addDeleteTableLayout, 0, 1);
            this._dataListTableLayout.Controls.Add(this._mealListBox, 0, 0);
            this._dataListTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._dataListTableLayout.Location = new System.Drawing.Point(3, 3);
            this._dataListTableLayout.Name = "_dataListTableLayout";
            this._dataListTableLayout.RowCount = 2;
            this._dataListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this._dataListTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._dataListTableLayout.Size = new System.Drawing.Size(321, 443);
            this._dataListTableLayout.TabIndex = 0;
            // 
            // _addDeleteTableLayout
            // 
            this._addDeleteTableLayout.ColumnCount = 2;
            this._addDeleteTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addDeleteTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addDeleteTableLayout.Controls.Add(this._addMealButton, 0, 0);
            this._addDeleteTableLayout.Controls.Add(this._deleteMealButton, 1, 0);
            this._addDeleteTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addDeleteTableLayout.Location = new System.Drawing.Point(3, 401);
            this._addDeleteTableLayout.Name = "_addDeleteTableLayout";
            this._addDeleteTableLayout.RowCount = 1;
            this._addDeleteTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._addDeleteTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this._addDeleteTableLayout.Size = new System.Drawing.Size(315, 39);
            this._addDeleteTableLayout.TabIndex = 0;
            // 
            // _addMealButton
            // 
            this._addMealButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._addMealButton.Location = new System.Drawing.Point(3, 8);
            this._addMealButton.Name = "_addMealButton";
            this._addMealButton.Size = new System.Drawing.Size(111, 23);
            this._addMealButton.TabIndex = 0;
            this._addMealButton.Text = "Add New Meal";
            this._addMealButton.UseVisualStyleBackColor = true;
            // 
            // _deleteMealButton
            // 
            this._deleteMealButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._deleteMealButton.Location = new System.Drawing.Point(188, 8);
            this._deleteMealButton.Name = "_deleteMealButton";
            this._deleteMealButton.Size = new System.Drawing.Size(124, 23);
            this._deleteMealButton.TabIndex = 1;
            this._deleteMealButton.Text = "Delete Selected Meal";
            this._deleteMealButton.UseVisualStyleBackColor = true;
            // 
            // _mealListBox
            // 
            this._mealListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealListBox.FormattingEnabled = true;
            this._mealListBox.ItemHeight = 12;
            this._mealListBox.Location = new System.Drawing.Point(3, 3);
            this._mealListBox.Name = "_mealListBox";
            this._mealListBox.Size = new System.Drawing.Size(315, 392);
            this._mealListBox.TabIndex = 1;
            this._mealListBox.SelectedIndexChanged += new System.EventHandler(this.SelectMeal);
            // 
            // _mealEditGroupBox
            // 
            this._mealEditGroupBox.Controls.Add(this._tableLayoutPanel1);
            this._mealEditGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealEditGroupBox.Location = new System.Drawing.Point(330, 3);
            this._mealEditGroupBox.Name = "_mealEditGroupBox";
            this._mealEditGroupBox.Size = new System.Drawing.Size(604, 443);
            this._mealEditGroupBox.TabIndex = 1;
            this._mealEditGroupBox.TabStop = false;
            this._mealEditGroupBox.Text = "Edit Meal";
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 1;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel4, 0, 2);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel3, 0, 1);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel2, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._descriptionLabel, 0, 3);
            this._tableLayoutPanel1.Controls.Add(this._mealDescriptionTextBox, 0, 4);
            this._tableLayoutPanel1.Controls.Add(this._mealSaveButton, 0, 5);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 6;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(598, 422);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _tableLayoutPanel4
            // 
            this._tableLayoutPanel4.ColumnCount = 3;
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel4.Controls.Add(this._mealImageLabel, 0, 0);
            this._tableLayoutPanel4.Controls.Add(this._mealImagePathTextBox, 1, 0);
            this._tableLayoutPanel4.Controls.Add(this._browseButton, 2, 0);
            this._tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel4.Location = new System.Drawing.Point(3, 129);
            this._tableLayoutPanel4.Name = "_tableLayoutPanel4";
            this._tableLayoutPanel4.RowCount = 1;
            this._tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel4.Size = new System.Drawing.Size(592, 57);
            this._tableLayoutPanel4.TabIndex = 7;
            // 
            // _mealImageLabel
            // 
            this._mealImageLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._mealImageLabel.AutoSize = true;
            this._mealImageLabel.Font = new System.Drawing.Font("PMingLiU", 9F);
            this._mealImageLabel.Location = new System.Drawing.Point(3, 22);
            this._mealImageLabel.Name = "_mealImageLabel";
            this._mealImageLabel.Size = new System.Drawing.Size(138, 12);
            this._mealImageLabel.TabIndex = 1;
            this._mealImageLabel.Text = "Meal Image Relative Path(*)";
            // 
            // _mealImagePathTextBox
            // 
            this._mealImagePathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._mealImagePathTextBox.Location = new System.Drawing.Point(151, 17);
            this._mealImagePathTextBox.Name = "_mealImagePathTextBox";
            this._mealImagePathTextBox.Size = new System.Drawing.Size(349, 22);
            this._mealImagePathTextBox.TabIndex = 2;
            // 
            // _browseButton
            // 
            this._browseButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._browseButton.Location = new System.Drawing.Point(506, 17);
            this._browseButton.Name = "_browseButton";
            this._browseButton.Size = new System.Drawing.Size(75, 23);
            this._browseButton.TabIndex = 3;
            this._browseButton.Text = "Browse";
            this._browseButton.UseVisualStyleBackColor = true;
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 5;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this._tableLayoutPanel3.Controls.Add(this._mealPriceLabel, 0, 0);
            this._tableLayoutPanel3.Controls.Add(this._mealPriceTextBox, 1, 0);
            this._tableLayoutPanel3.Controls.Add(this._dollarLabel, 2, 0);
            this._tableLayoutPanel3.Controls.Add(this._categoryComboBox, 4, 0);
            this._tableLayoutPanel3.Controls.Add(this._categoryLabel, 3, 0);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(3, 66);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 1;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(592, 57);
            this._tableLayoutPanel3.TabIndex = 6;
            // 
            // _mealPriceLabel
            // 
            this._mealPriceLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._mealPriceLabel.AutoSize = true;
            this._mealPriceLabel.Font = new System.Drawing.Font("PMingLiU", 9F);
            this._mealPriceLabel.Location = new System.Drawing.Point(3, 22);
            this._mealPriceLabel.Name = "_mealPriceLabel";
            this._mealPriceLabel.Size = new System.Drawing.Size(68, 12);
            this._mealPriceLabel.TabIndex = 1;
            this._mealPriceLabel.Text = "Meal Price(*)";
            // 
            // _mealPriceTextBox
            // 
            this._mealPriceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._mealPriceTextBox.Location = new System.Drawing.Point(91, 17);
            this._mealPriceTextBox.Name = "_mealPriceTextBox";
            this._mealPriceTextBox.Size = new System.Drawing.Size(171, 22);
            this._mealPriceTextBox.TabIndex = 2;
            // 
            // _dollarLabel
            // 
            this._dollarLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._dollarLabel.AutoSize = true;
            this._dollarLabel.Location = new System.Drawing.Point(268, 22);
            this._dollarLabel.Name = "_dollarLabel";
            this._dollarLabel.Size = new System.Drawing.Size(28, 12);
            this._dollarLabel.TabIndex = 3;
            this._dollarLabel.Text = "NTD";
            // 
            // _categoryComboBox
            // 
            this._categoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._categoryComboBox.FormattingEnabled = true;
            this._categoryComboBox.Location = new System.Drawing.Point(415, 18);
            this._categoryComboBox.Name = "_categoryComboBox";
            this._categoryComboBox.Size = new System.Drawing.Size(174, 20);
            this._categoryComboBox.TabIndex = 4;
            this._categoryComboBox.SelectedIndexChanged += new System.EventHandler(this.ChangeInformation);
            // 
            // _categoryLabel
            // 
            this._categoryLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._categoryLabel.AutoSize = true;
            this._categoryLabel.Location = new System.Drawing.Point(327, 22);
            this._categoryLabel.Name = "_categoryLabel";
            this._categoryLabel.Size = new System.Drawing.Size(62, 12);
            this._categoryLabel.TabIndex = 5;
            this._categoryLabel.Text = "Category(*)";
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 2;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._tableLayoutPanel2.Controls.Add(this._mealNameLabel, 0, 0);
            this._tableLayoutPanel2.Controls.Add(this._mealNameTextBox, 1, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 1;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(592, 57);
            this._tableLayoutPanel2.TabIndex = 5;
            // 
            // _mealNameLabel
            // 
            this._mealNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._mealNameLabel.AutoSize = true;
            this._mealNameLabel.Font = new System.Drawing.Font("PMingLiU", 9F);
            this._mealNameLabel.Location = new System.Drawing.Point(3, 22);
            this._mealNameLabel.Name = "_mealNameLabel";
            this._mealNameLabel.Size = new System.Drawing.Size(72, 12);
            this._mealNameLabel.TabIndex = 1;
            this._mealNameLabel.Text = "Meal Name(*)";
            // 
            // _mealNameTextBox
            // 
            this._mealNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._mealNameTextBox.Location = new System.Drawing.Point(91, 17);
            this._mealNameTextBox.Name = "_mealNameTextBox";
            this._mealNameTextBox.Size = new System.Drawing.Size(498, 22);
            this._mealNameTextBox.TabIndex = 2;
            // 
            // _descriptionLabel
            // 
            this._descriptionLabel.AutoSize = true;
            this._descriptionLabel.Location = new System.Drawing.Point(3, 189);
            this._descriptionLabel.Name = "_descriptionLabel";
            this._descriptionLabel.Size = new System.Drawing.Size(58, 12);
            this._descriptionLabel.TabIndex = 8;
            this._descriptionLabel.Text = "Description";
            // 
            // _mealDescriptionTextBox
            // 
            this._mealDescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealDescriptionTextBox.Location = new System.Drawing.Point(3, 213);
            this._mealDescriptionTextBox.Multiline = true;
            this._mealDescriptionTextBox.Name = "_mealDescriptionTextBox";
            this._mealDescriptionTextBox.Size = new System.Drawing.Size(592, 162);
            this._mealDescriptionTextBox.TabIndex = 9;
            // 
            // _mealSaveButton
            // 
            this._mealSaveButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._mealSaveButton.Enabled = false;
            this._mealSaveButton.Location = new System.Drawing.Point(520, 388);
            this._mealSaveButton.Name = "_mealSaveButton";
            this._mealSaveButton.Size = new System.Drawing.Size(75, 23);
            this._mealSaveButton.TabIndex = 10;
            this._mealSaveButton.Text = "Save";
            this._mealSaveButton.UseVisualStyleBackColor = true;
            this._mealSaveButton.Click += new System.EventHandler(this.ClickSaveButton);
            // 
            // _categoryManager
            // 
            this._categoryManager.Controls.Add(this._categoryManageTableLayout);
            this._categoryManager.Location = new System.Drawing.Point(4, 22);
            this._categoryManager.Name = "_categoryManager";
            this._categoryManager.Padding = new System.Windows.Forms.Padding(3);
            this._categoryManager.Size = new System.Drawing.Size(943, 455);
            this._categoryManager.TabIndex = 1;
            this._categoryManager.Text = "Category Manager";
            this._categoryManager.UseVisualStyleBackColor = true;
            // 
            // _categoryManageTableLayout
            // 
            this._categoryManageTableLayout.ColumnCount = 2;
            this._categoryManageTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this._categoryManageTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this._categoryManageTableLayout.Controls.Add(this._categoryTableLayoutPanel1, 0, 0);
            this._categoryManageTableLayout.Controls.Add(this._categoryGroupBox, 1, 0);
            this._categoryManageTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryManageTableLayout.Location = new System.Drawing.Point(3, 3);
            this._categoryManageTableLayout.Name = "_categoryManageTableLayout";
            this._categoryManageTableLayout.RowCount = 1;
            this._categoryManageTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._categoryManageTableLayout.Size = new System.Drawing.Size(937, 449);
            this._categoryManageTableLayout.TabIndex = 0;
            // 
            // _categoryTableLayoutPanel1
            // 
            this._categoryTableLayoutPanel1.ColumnCount = 1;
            this._categoryTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._categoryTableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._categoryTableLayoutPanel1.Controls.Add(this._categoryListBox, 0, 0);
            this._categoryTableLayoutPanel1.Controls.Add(this._addDeleteTableLayoutPanel, 0, 1);
            this._categoryTableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryTableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this._categoryTableLayoutPanel1.Name = "_categoryTableLayoutPanel1";
            this._categoryTableLayoutPanel1.RowCount = 2;
            this._categoryTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._categoryTableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._categoryTableLayoutPanel1.Size = new System.Drawing.Size(321, 443);
            this._categoryTableLayoutPanel1.TabIndex = 0;
            // 
            // _categoryListBox
            // 
            this._categoryListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryListBox.FormattingEnabled = true;
            this._categoryListBox.ItemHeight = 12;
            this._categoryListBox.Location = new System.Drawing.Point(3, 3);
            this._categoryListBox.Name = "_categoryListBox";
            this._categoryListBox.Size = new System.Drawing.Size(315, 370);
            this._categoryListBox.TabIndex = 0;
            this._categoryListBox.SelectedIndexChanged += new System.EventHandler(this.SelectCategory);
            // 
            // _addDeleteTableLayoutPanel
            // 
            this._addDeleteTableLayoutPanel.ColumnCount = 2;
            this._addDeleteTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addDeleteTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._addDeleteTableLayoutPanel.Controls.Add(this._addCategoryButton, 0, 0);
            this._addDeleteTableLayoutPanel.Controls.Add(this._deleteCategoryButton, 1, 0);
            this._addDeleteTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addDeleteTableLayoutPanel.Location = new System.Drawing.Point(3, 379);
            this._addDeleteTableLayoutPanel.Name = "_addDeleteTableLayoutPanel";
            this._addDeleteTableLayoutPanel.RowCount = 1;
            this._addDeleteTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._addDeleteTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this._addDeleteTableLayoutPanel.Size = new System.Drawing.Size(315, 61);
            this._addDeleteTableLayoutPanel.TabIndex = 1;
            // 
            // _addCategoryButton
            // 
            this._addCategoryButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._addCategoryButton.Location = new System.Drawing.Point(3, 19);
            this._addCategoryButton.Name = "_addCategoryButton";
            this._addCategoryButton.Size = new System.Drawing.Size(112, 23);
            this._addCategoryButton.TabIndex = 0;
            this._addCategoryButton.Text = "Add Category";
            this._addCategoryButton.UseVisualStyleBackColor = true;
            // 
            // _deleteCategoryButton
            // 
            this._deleteCategoryButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._deleteCategoryButton.Location = new System.Drawing.Point(183, 19);
            this._deleteCategoryButton.Name = "_deleteCategoryButton";
            this._deleteCategoryButton.Size = new System.Drawing.Size(129, 23);
            this._deleteCategoryButton.TabIndex = 1;
            this._deleteCategoryButton.Text = "Delete Selected Category";
            this._deleteCategoryButton.UseVisualStyleBackColor = true;
            // 
            // _categoryGroupBox
            // 
            this._categoryGroupBox.Controls.Add(this.tableLayoutPanel1);
            this._categoryGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._categoryGroupBox.Location = new System.Drawing.Point(330, 3);
            this._categoryGroupBox.Name = "_categoryGroupBox";
            this._categoryGroupBox.Size = new System.Drawing.Size(604, 443);
            this._categoryGroupBox.TabIndex = 1;
            this._categoryGroupBox.TabStop = false;
            this._categoryGroupBox.Text = "Category";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._mealUsedCategoryList, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._categorySaveButton, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(598, 422);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this._categoryNameTextBox, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this._categoryNameLabel, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(592, 36);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // _categoryNameLabel
            // 
            this._categoryNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._categoryNameLabel.AutoSize = true;
            this._categoryNameLabel.Location = new System.Drawing.Point(3, 12);
            this._categoryNameLabel.Name = "_categoryNameLabel";
            this._categoryNameLabel.Size = new System.Drawing.Size(95, 12);
            this._categoryNameLabel.TabIndex = 0;
            this._categoryNameLabel.Text = "Category Name (*)";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Meals using this category:";
            // 
            // _mealUsedCategoryList
            // 
            this._mealUsedCategoryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealUsedCategoryList.FormattingEnabled = true;
            this._mealUsedCategoryList.ItemHeight = 12;
            this._mealUsedCategoryList.Location = new System.Drawing.Point(3, 87);
            this._mealUsedCategoryList.Name = "_mealUsedCategoryList";
            this._mealUsedCategoryList.Size = new System.Drawing.Size(592, 268);
            this._mealUsedCategoryList.TabIndex = 2;
            // 
            // _categorySaveButton
            // 
            this._categorySaveButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._categorySaveButton.Location = new System.Drawing.Point(520, 378);
            this._categorySaveButton.Name = "_categorySaveButton";
            this._categorySaveButton.Size = new System.Drawing.Size(75, 23);
            this._categorySaveButton.TabIndex = 3;
            this._categorySaveButton.Text = "Save";
            this._categorySaveButton.UseVisualStyleBackColor = true;
            // 
            // _categoryNameTextBox
            // 
            this._categoryNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this._categoryNameTextBox.Location = new System.Drawing.Point(151, 7);
            this._categoryNameTextBox.Name = "_categoryNameTextBox";
            this._categoryNameTextBox.Size = new System.Drawing.Size(438, 22);
            this._categoryNameTextBox.TabIndex = 3;
            // 
            // POSRestaurantSideForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 481);
            this.Controls.Add(this._managerTabControl);
            this.Name = "POSRestaurantSideForm";
            this.Text = "POSRestaurantSideForm";
            this._managerTabControl.ResumeLayout(false);
            this._mealManager.ResumeLayout(false);
            this._mealManagerTableLayout.ResumeLayout(false);
            this._dataListTableLayout.ResumeLayout(false);
            this._addDeleteTableLayout.ResumeLayout(false);
            this._mealEditGroupBox.ResumeLayout(false);
            this._tableLayoutPanel1.ResumeLayout(false);
            this._tableLayoutPanel1.PerformLayout();
            this._tableLayoutPanel4.ResumeLayout(false);
            this._tableLayoutPanel4.PerformLayout();
            this._tableLayoutPanel3.ResumeLayout(false);
            this._tableLayoutPanel3.PerformLayout();
            this._tableLayoutPanel2.ResumeLayout(false);
            this._tableLayoutPanel2.PerformLayout();
            this._categoryManager.ResumeLayout(false);
            this._categoryManageTableLayout.ResumeLayout(false);
            this._categoryTableLayoutPanel1.ResumeLayout(false);
            this._addDeleteTableLayoutPanel.ResumeLayout(false);
            this._categoryGroupBox.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _managerTabControl;
        private System.Windows.Forms.TabPage _mealManager;
        private System.Windows.Forms.TableLayoutPanel _mealManagerTableLayout;
        private System.Windows.Forms.TabPage _categoryManager;
        private System.Windows.Forms.TableLayoutPanel _categoryManageTableLayout;
        private System.Windows.Forms.TableLayoutPanel _dataListTableLayout;
        private System.Windows.Forms.TableLayoutPanel _addDeleteTableLayout;
        private System.Windows.Forms.Button _addMealButton;
        private System.Windows.Forms.Button _deleteMealButton;
        private System.Windows.Forms.ListBox _mealListBox;
        private System.Windows.Forms.GroupBox _mealEditGroupBox;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel4;
        private System.Windows.Forms.Label _mealImageLabel;
        private System.Windows.Forms.TextBox _mealImagePathTextBox;
        private System.Windows.Forms.Button _browseButton;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.Label _mealPriceLabel;
        private System.Windows.Forms.TextBox _mealPriceTextBox;
        private System.Windows.Forms.Label _dollarLabel;
        private System.Windows.Forms.ComboBox _categoryComboBox;
        private System.Windows.Forms.Label _categoryLabel;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.Label _mealNameLabel;
        private System.Windows.Forms.TextBox _mealNameTextBox;
        private System.Windows.Forms.Label _descriptionLabel;
        private System.Windows.Forms.TextBox _mealDescriptionTextBox;
        private System.Windows.Forms.Button _mealSaveButton;
        private System.Windows.Forms.TableLayoutPanel _categoryTableLayoutPanel1;
        private System.Windows.Forms.GroupBox _categoryGroupBox;
        private System.Windows.Forms.ListBox _categoryListBox;
        private System.Windows.Forms.TableLayoutPanel _addDeleteTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label _categoryNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox _mealUsedCategoryList;
        private System.Windows.Forms.Button _categorySaveButton;
        private System.Windows.Forms.Button _addCategoryButton;
        private System.Windows.Forms.Button _deleteCategoryButton;
        private System.Windows.Forms.TextBox _categoryNameTextBox;
    }
}