﻿namespace POSCustomerSide
{
    partial class MealButtonUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._mealButton1 = new System.Windows.Forms.Button();
            this._mealButton2 = new System.Windows.Forms.Button();
            this._mealButton3 = new System.Windows.Forms.Button();
            this._mealButton4 = new System.Windows.Forms.Button();
            this._mealButton5 = new System.Windows.Forms.Button();
            this._mealButton6 = new System.Windows.Forms.Button();
            this._mealButton7 = new System.Windows.Forms.Button();
            this._mealButton8 = new System.Windows.Forms.Button();
            this._mealButton9 = new System.Windows.Forms.Button();
            this._tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 3;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tableLayoutPanel3.Controls.Add(this._mealButton1, 0, 0);
            this._tableLayoutPanel3.Controls.Add(this._mealButton2, 1, 0);
            this._tableLayoutPanel3.Controls.Add(this._mealButton3, 2, 0);
            this._tableLayoutPanel3.Controls.Add(this._mealButton4, 0, 1);
            this._tableLayoutPanel3.Controls.Add(this._mealButton5, 1, 1);
            this._tableLayoutPanel3.Controls.Add(this._mealButton6, 2, 1);
            this._tableLayoutPanel3.Controls.Add(this._mealButton7, 0, 2);
            this._tableLayoutPanel3.Controls.Add(this._mealButton8, 1, 2);
            this._tableLayoutPanel3.Controls.Add(this._mealButton9, 2, 2);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 3;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(314, 245);
            this._tableLayoutPanel3.TabIndex = 2;
            // 
            // _mealButton1
            // 
            this._mealButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton1.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton1.Location = new System.Drawing.Point(3, 3);
            this._mealButton1.Name = "_mealButton1";
            this._mealButton1.Size = new System.Drawing.Size(98, 75);
            this._mealButton1.TabIndex = 0;
            this._mealButton1.Text = "button1";
            this._mealButton1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton1.UseVisualStyleBackColor = true;
            // 
            // _mealButton2
            // 
            this._mealButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton2.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton2.Location = new System.Drawing.Point(107, 3);
            this._mealButton2.Name = "_mealButton2";
            this._mealButton2.Size = new System.Drawing.Size(98, 75);
            this._mealButton2.TabIndex = 1;
            this._mealButton2.Text = "button2";
            this._mealButton2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton2.UseVisualStyleBackColor = true;
            // 
            // _mealButton3
            // 
            this._mealButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton3.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton3.Location = new System.Drawing.Point(211, 3);
            this._mealButton3.Name = "_mealButton3";
            this._mealButton3.Size = new System.Drawing.Size(100, 75);
            this._mealButton3.TabIndex = 2;
            this._mealButton3.Text = "button3";
            this._mealButton3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton3.UseVisualStyleBackColor = true;
            // 
            // _mealButton4
            // 
            this._mealButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton4.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton4.Location = new System.Drawing.Point(3, 84);
            this._mealButton4.Name = "_mealButton4";
            this._mealButton4.Size = new System.Drawing.Size(98, 75);
            this._mealButton4.TabIndex = 3;
            this._mealButton4.Text = "button4";
            this._mealButton4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton4.UseVisualStyleBackColor = true;
            // 
            // _mealButton5
            // 
            this._mealButton5.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton5.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton5.Location = new System.Drawing.Point(107, 84);
            this._mealButton5.Name = "_mealButton5";
            this._mealButton5.Size = new System.Drawing.Size(98, 75);
            this._mealButton5.TabIndex = 4;
            this._mealButton5.Text = "button5";
            this._mealButton5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton5.UseVisualStyleBackColor = true;
            // 
            // _mealButton6
            // 
            this._mealButton6.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton6.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton6.Location = new System.Drawing.Point(211, 84);
            this._mealButton6.Name = "_mealButton6";
            this._mealButton6.Size = new System.Drawing.Size(100, 75);
            this._mealButton6.TabIndex = 5;
            this._mealButton6.Text = "button6";
            this._mealButton6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton6.UseVisualStyleBackColor = true;
            // 
            // _mealButton7
            // 
            this._mealButton7.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton7.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton7.Location = new System.Drawing.Point(3, 165);
            this._mealButton7.Name = "_mealButton7";
            this._mealButton7.Size = new System.Drawing.Size(98, 77);
            this._mealButton7.TabIndex = 6;
            this._mealButton7.Text = "button7";
            this._mealButton7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton7.UseVisualStyleBackColor = true;
            // 
            // _mealButton8
            // 
            this._mealButton8.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton8.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton8.Location = new System.Drawing.Point(107, 165);
            this._mealButton8.Name = "_mealButton8";
            this._mealButton8.Size = new System.Drawing.Size(98, 77);
            this._mealButton8.TabIndex = 7;
            this._mealButton8.Text = "button8";
            this._mealButton8.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton8.UseVisualStyleBackColor = true;
            // 
            // _mealButton9
            // 
            this._mealButton9.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealButton9.Font = new System.Drawing.Font("PMingLiU", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._mealButton9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this._mealButton9.Location = new System.Drawing.Point(211, 165);
            this._mealButton9.Name = "_mealButton9";
            this._mealButton9.Size = new System.Drawing.Size(100, 77);
            this._mealButton9.TabIndex = 8;
            this._mealButton9.Text = "button9";
            this._mealButton9.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._mealButton9.UseVisualStyleBackColor = true;
            // 
            // MealButtonUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tableLayoutPanel3);
            this.Name = "MealButtonUserControl";
            this.Size = new System.Drawing.Size(314, 245);
            this._tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.Button _mealButton1;
        private System.Windows.Forms.Button _mealButton2;
        private System.Windows.Forms.Button _mealButton3;
        private System.Windows.Forms.Button _mealButton4;
        private System.Windows.Forms.Button _mealButton5;
        private System.Windows.Forms.Button _mealButton6;
        private System.Windows.Forms.Button _mealButton7;
        private System.Windows.Forms.Button _mealButton8;
        private System.Windows.Forms.Button _mealButton9;
    }
}
