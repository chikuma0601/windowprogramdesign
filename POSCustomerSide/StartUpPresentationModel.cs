﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class StartUpPresentationModel
    {
        private bool _customerButtonEnable = true;
        private bool _restaurantButtonEnable = true;

        // get customer enable
        public bool IsCustomerButtonEnable()
        {
            return _customerButtonEnable;
        }

        // get restaurant enable
        public bool IsRestaurantButtonEnable()
        {
            return _restaurantButtonEnable;
        }

        // Set the customer enable
        public void SetCustomerEnable(bool enable)
        {
            _customerButtonEnable = enable;
        }

        // set the restaurant enable
        public void SetRestaurantEnable(bool enable)
        {
            _restaurantButtonEnable = enable;
        }
    }
}
