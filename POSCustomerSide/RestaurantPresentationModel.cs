﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class RestaurantPresentationModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private POSCustomerSideModel _posModel;

        private Meal _selectedMeal;
        private string _selectedCategory;
        private bool _saveMealButtonEnable = false;

        // MealManager, will be displayed on Form
        private string _mealName;
        private int _mealPrice;
        private string _mealImagePath;
        private string _mealDescription;
        private int _mealSelectedCategoryIndex;

        // Category stuffs
        private string _categoryName;

        // Contructor
        public RestaurantPresentationModel(POSCustomerSideModel posModel)
        {
            _posModel = posModel;
        }

        public Meal GetSelectedMeal
        {
            get
            {
                return _selectedMeal;
            }
        }

        public BindingList<Meal> Meals
        {
            get
            {
                return _posModel.MealsInFile;
            }
        }

        public string MealName
        {
            get
            {
                return _mealName;
            }
            set
            {
                _mealName = value;
                Notify(nameof(MealName));
            }
        }

        public string MealPrice
        {
            get
            {
                return _mealPrice.ToString();
            }
            set
            {
                _mealPrice = Int32.Parse(value);
                Notify(nameof(MealPrice));
            }
        }

        public string MealImagePath
        {
            get
            {
                return _mealImagePath;
            }
            set
            {
                _mealImagePath = value;
                Notify(nameof(MealImagePath));
            }
        }

        public string MealDescription
        {
            get
            {
                return _mealDescription;
            }
            set
            {
                _mealDescription = value;
                Notify(nameof(MealDescription));
            }
        }

        public int MealSelectedCategoryIndex
        {
            get
            {
                return _mealSelectedCategoryIndex;
            }
            set
            {
                _mealSelectedCategoryIndex = value;
                Notify(nameof(MealSelectedCategoryIndex));
            }
        }

        public bool SaveMealButtonEnable
        {
            get
            {
                return _saveMealButtonEnable;
            }
            set
            {
                _saveMealButtonEnable = value;
                Notify(nameof(SaveMealButtonEnable));
            }
        }

        public string CategoryName
        {
            get
            {
                return _categoryName;
            }
            set
            {
                _categoryName = value;
                Notify(nameof(CategoryName));
            }
        }

        public string[] SelectedMeal
        {
            get
            {
                string[] mealInformation = { _mealName, _mealPrice.ToString(), _mealImagePath, _mealDescription, _selectedMeal.Category };
                return mealInformation;
            }
        }

        // Set the mealName to the current selected meal
        public void SetInformationToSelected()
        {
            _mealName = _selectedMeal.Name;
            _mealPrice = _selectedMeal.Price;
            _mealImagePath = _selectedMeal.ImageName;
            _mealDescription = _selectedMeal.Description;
            _mealSelectedCategoryIndex = _posModel.GetCategoryIndex(_selectedMeal.Category);
            Notify(nameof(MealName));
            Notify(nameof(MealPrice));
            Notify(nameof(MealImagePath));
            Notify(nameof(MealDescription));
            Notify(nameof(MealSelectedCategoryIndex));
        }

        // Set the category Name to the current selected one
        public void SetCategoryInformationToSelected()
        {
            _categoryName = _selectedCategory;
            Notify(nameof(CategoryName));
        }

        // All of the categoires
        public BindingList<string> Categories
        {
            get
            {
                return _posModel.MenuCategory;
            }
        }

        // Throws an event of propertyChanged
        public void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        // Set the selected meal by index, using the BindingList in model
        public void SetSelectedMeal(int index)
        {
            if (index != -1)
                _selectedMeal = _posModel.MealsInFile[index].GetClone();
        }

        // Set the selected category by index, string does not need GetClone() :)
        public void SetSelectedCategory(int index)
        {
            if (index != -1)
                _selectedCategory = _posModel.MenuCategory[index];
        }

        // Save the meal into Meal
        public void SaveMeal()
        {
            _selectedMeal.Name = _mealName;
            _selectedMeal.Price = _mealPrice;
            _selectedMeal.Description = _mealDescription;
            _selectedMeal.ImageName = _mealImagePath;
        }
    }
}
