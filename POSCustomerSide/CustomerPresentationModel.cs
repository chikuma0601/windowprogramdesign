﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

namespace POSCustomerSide
{
    public class CustomerPresentationModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private POSCustomerSideModel _posModel;
        private readonly int _maxButtonOnPage = 9;
        private readonly string _currencySymbol = "$";
        private readonly string _imageRelativePath = "\\Resources\\";
        private readonly string _priceString = "Price: ";
        private bool _nextPageEnable = true;
        private bool _previousPageEnable = true;
        private bool _addButtonEnable = false;

        public CustomerPresentationModel(POSCustomerSideModel posModel)
        {
            _posModel = posModel;
        }

        // call setMealButtonMap in model to initialize the button by menu and setup the text on button
        public void InitializeButtonMap()
        {
            _posModel.SetMealButtonMap();
        }

        // Get a list contains "should the button be visible"
        public List<bool> GetMealButtonVisible()
        {
            List<bool> mealButtonVisible = new List<bool>();
            for (int i = 0; i < _maxButtonOnPage; ++i)
            {
                mealButtonVisible.Add(_posModel.GetMealOnPage() > i);
            }
            return mealButtonVisible;
        }

        // Gets the text on the meal button
        public List<string> GetButtonText
        {
            get
            {
                return _posModel.GetButtonText();
            }
        }

        // Get page label
        public String GetPageLabel
        {
            get
            {
                return _posModel.GetPageLabelText();
            }
        }

        // Set the next/prev page enable for Form to refresh
        public void SetPageButtonEnable()
        {
            if (_posModel.IsAtFirstPage())
                _previousPageEnable = false;
            else
                _previousPageEnable = true;
            if (_posModel.IsAtLastPage())
                _nextPageEnable = false;
            else
                _nextPageEnable = true;
            Notify(nameof(IsNextPageEnable));
            Notify(nameof(IsPreviousPageEnable));
        }

        // Get nextPageEnable
        public bool IsNextPageEnable
        {
            get
            {
                return _nextPageEnable;
            }
        }

        // Get previousPageEnable
        public bool IsPreviousPageEnable
        {
            get
            {
                return _previousPageEnable;
            }
        }

        // Returns addButtonEnable
        public bool IsAddButtonEnable
        {
            get
            {
                return _addButtonEnable;
            }
        }

        // Get the Price: <TotalPrice> <currencySign>
        public String GetPriceText()
        {
            return _priceString + _posModel.GetTotalPrice() + _currencySymbol;
        }

        // when add button is clicked...
        public List<String> AddMealToOrder()
        {
            _posModel.AddToOrder();
            return _posModel.GetAddedOrder();
        }

        // Set the selected button to index
        public void SetSelectedMeal(int index)
        {
            _posModel.ConvertButtonIndexToMealIndex(index);
            RefreshAddButtonEnable();
        }

        // filp to nextPage when nextPage
        public void FlipNextPage()
        {
            _posModel.FlipNextPage();
        }

        // filp to previous Page when prevPage
        public void FlipPreviousPage()
        {
            _posModel.FlipPreviousPage();
        }

        // reset the selected meal to -1
        public void ResetSelectedMeal()
        {
            _posModel.ResetSelectedMeal();
        }

        // Delete the order by index
        public void DeleteOrder(int orderIndex)
        {
            _posModel.DeleteOrderByIndex(orderIndex);
        }

        // Set the addButtonEnable accroding to posModel.GetAddedOrder
        public void RefreshAddButtonEnable()
        {
            if (_posModel.SelectedMealMapIndex == -1)
                _addButtonEnable = false;
            else
                _addButtonEnable = true;
            Notify(nameof(IsAddButtonEnable));
        }

        // Get the absolute image path on the current menu
        public List<string> GetMealImageAbsolutePath()
        {
            List<string> imagePath = _posModel.GetMealImageName();
            string projectPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            for (int i = 0; i < imagePath.Count; ++i)
            {
                imagePath[i] = projectPath + _imageRelativePath + imagePath[i];
            }
            return imagePath;
        }

        // Get the description of the selected meal
        public string GetDescription()
        {
            string description = _posModel.GetSelectedMealDescription();
            if (description == null)
                return "";
            return _posModel.GetSelectedMealDescription();
        }

        // Throws an event of propertyChanged
        private void Notify(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        // get the bindingList of the orders
        public BindingList<SingleOrder> SingleOrders
        {
            get
            {
                return _posModel.Order.Orders;
            }
        }

        //***************TESTING***********************
        public BindingList<int> Quantity
        {
            get
            {
                return _posModel.Order.Quantity;
            }
        }

        public BindingList<string> Category
        {
            get
            {
                return _posModel.MenuCategory;
            }
        }

        // Reset if tab changed
        public void ResetPage()
        {
            _posModel.ResetPage();
        }

        // Tab index changed, update currentTab in model
        public void ChangeTabIndex(int index)
        {
            _posModel.ChangeCurrentCategory(index);
        }

        // Updates meal information
        public void UpdateMealInformation()
        {
            _posModel.UpdateMealInformation();
        }
    }
}
