﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POSCustomerSide
{
    class Program
    {
        // This is Main ... yep.
        static void Main(string[] args)
        {
            StartUpForm startUp = new StartUpForm();
            Application.EnableVisualStyles();
            Application.Run(startUp);
        }
    }
}
