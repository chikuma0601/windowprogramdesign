﻿namespace POSCustomerSide
{
    partial class POSCustomerSideForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._meals = new System.Windows.Forms.GroupBox();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this._addButton = new System.Windows.Forms.Button();
            this._nextPageButton = new System.Windows.Forms.Button();
            this._previousPageButton = new System.Windows.Forms.Button();
            this._pageLabel = new System.Windows.Forms.Label();
            this._mealDescription = new System.Windows.Forms.RichTextBox();
            this._mealTabControl = new System.Windows.Forms.TabControl();
            this._tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this._orderDataGridView = new System.Windows.Forms.DataGridView();
            this._orderDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this._totalLabel = new System.Windows.Forms.Label();
            this._tableLayoutPanel1.SuspendLayout();
            this._meals.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._tableLayoutPanel4.SuspendLayout();
            this._tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._orderDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 2;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel1.Controls.Add(this._meals, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel5, 1, 0);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 1;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _meals
            // 
            this._meals.Controls.Add(this._tableLayoutPanel2);
            this._meals.Dock = System.Windows.Forms.DockStyle.Fill;
            this._meals.Location = new System.Drawing.Point(3, 3);
            this._meals.Name = "_meals";
            this._meals.Size = new System.Drawing.Size(394, 444);
            this._meals.TabIndex = 0;
            this._meals.TabStop = false;
            this._meals.Text = "Meals";
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 1;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.Controls.Add(this._tableLayoutPanel4, 0, 2);
            this._tableLayoutPanel2.Controls.Add(this._mealDescription, 0, 1);
            this._tableLayoutPanel2.Controls.Add(this._mealTabControl, 0, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(3, 18);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 3;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(388, 423);
            this._tableLayoutPanel2.TabIndex = 0;
            // 
            // _tableLayoutPanel4
            // 
            this._tableLayoutPanel4.ColumnCount = 4;
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.85098F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.44705F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.85098F));
            this._tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.85098F));
            this._tableLayoutPanel4.Controls.Add(this._addButton, 3, 0);
            this._tableLayoutPanel4.Controls.Add(this._nextPageButton, 3, 1);
            this._tableLayoutPanel4.Controls.Add(this._previousPageButton, 2, 1);
            this._tableLayoutPanel4.Controls.Add(this._pageLabel, 0, 1);
            this._tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel4.Location = new System.Drawing.Point(3, 362);
            this._tableLayoutPanel4.Name = "_tableLayoutPanel4";
            this._tableLayoutPanel4.RowCount = 2;
            this._tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel4.Size = new System.Drawing.Size(382, 58);
            this._tableLayoutPanel4.TabIndex = 1;
            // 
            // _addButton
            // 
            this._addButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._addButton.Location = new System.Drawing.Point(281, 3);
            this._addButton.Name = "_addButton";
            this._addButton.Size = new System.Drawing.Size(98, 23);
            this._addButton.TabIndex = 0;
            this._addButton.Text = "Add";
            this._addButton.UseVisualStyleBackColor = true;
            this._addButton.Click += new System.EventHandler(this.AddButtonClicked);
            // 
            // _nextPageButton
            // 
            this._nextPageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._nextPageButton.Location = new System.Drawing.Point(281, 32);
            this._nextPageButton.Name = "_nextPageButton";
            this._nextPageButton.Size = new System.Drawing.Size(98, 23);
            this._nextPageButton.TabIndex = 1;
            this._nextPageButton.Text = "NextPage";
            this._nextPageButton.UseVisualStyleBackColor = true;
            this._nextPageButton.Click += new System.EventHandler(this.ClickNextPage);
            // 
            // _previousPageButton
            // 
            this._previousPageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._previousPageButton.Location = new System.Drawing.Point(179, 32);
            this._previousPageButton.Name = "_previousPageButton";
            this._previousPageButton.Size = new System.Drawing.Size(96, 23);
            this._previousPageButton.TabIndex = 2;
            this._previousPageButton.Text = "PreviousPage";
            this._previousPageButton.UseVisualStyleBackColor = true;
            this._previousPageButton.Click += new System.EventHandler(this.ClickPreviousButton);
            // 
            // _pageLabel
            // 
            this._pageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this._pageLabel.AutoSize = true;
            this._pageLabel.Font = new System.Drawing.Font("PMingLiU", 12F);
            this._pageLabel.ForeColor = System.Drawing.SystemColors.Highlight;
            this._pageLabel.Location = new System.Drawing.Point(3, 42);
            this._pageLabel.Name = "_pageLabel";
            this._pageLabel.Size = new System.Drawing.Size(42, 16);
            this._pageLabel.TabIndex = 3;
            this._pageLabel.Text = "Page:";
            this._pageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _mealDescription
            // 
            this._mealDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealDescription.Location = new System.Drawing.Point(3, 299);
            this._mealDescription.Name = "_mealDescription";
            this._mealDescription.ReadOnly = true;
            this._mealDescription.Size = new System.Drawing.Size(382, 57);
            this._mealDescription.TabIndex = 2;
            this._mealDescription.Text = "";
            // 
            // _mealTabControl
            // 
            this._mealTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mealTabControl.Location = new System.Drawing.Point(3, 3);
            this._mealTabControl.Name = "_mealTabControl";
            this._mealTabControl.SelectedIndex = 0;
            this._mealTabControl.Size = new System.Drawing.Size(382, 290);
            this._mealTabControl.TabIndex = 3;
            this._mealTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.ChangeSelectedIndex);
            // 
            // _tableLayoutPanel5
            // 
            this._tableLayoutPanel5.ColumnCount = 1;
            this._tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel5.Controls.Add(this._orderDataGridView, 0, 0);
            this._tableLayoutPanel5.Controls.Add(this._totalLabel, 0, 1);
            this._tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel5.Location = new System.Drawing.Point(403, 3);
            this._tableLayoutPanel5.Name = "_tableLayoutPanel5";
            this._tableLayoutPanel5.RowCount = 2;
            this._tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this._tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this._tableLayoutPanel5.Size = new System.Drawing.Size(394, 444);
            this._tableLayoutPanel5.TabIndex = 1;
            // 
            // _orderDataGridView
            // 
            this._orderDataGridView.AllowUserToAddRows = false;
            this._orderDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this._orderDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._orderDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._orderDelete});
            this._orderDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this._orderDataGridView.GridColor = System.Drawing.SystemColors.Control;
            this._orderDataGridView.Location = new System.Drawing.Point(3, 3);
            this._orderDataGridView.Name = "_orderDataGridView";
            this._orderDataGridView.RowHeadersVisible = false;
            this._orderDataGridView.RowTemplate.Height = 24;
            this._orderDataGridView.Size = new System.Drawing.Size(388, 371);
            this._orderDataGridView.TabIndex = 0;
            // 
            // _orderDelete
            // 
            this._orderDelete.HeaderText = "Delete";
            this._orderDelete.Name = "_orderDelete";
            this._orderDelete.Text = "X";
            this._orderDelete.UseColumnTextForButtonValue = true;
            // 
            // _totalLabel
            // 
            this._totalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._totalLabel.AutoSize = true;
            this._totalLabel.Font = new System.Drawing.Font("Microsoft JhengHei", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this._totalLabel.ForeColor = System.Drawing.Color.Red;
            this._totalLabel.Location = new System.Drawing.Point(304, 409);
            this._totalLabel.Name = "_totalLabel";
            this._totalLabel.Size = new System.Drawing.Size(87, 35);
            this._totalLabel.TabIndex = 1;
            this._totalLabel.Text = "Total:";
            // 
            // POSCustomerSideForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Name = "POSCustomerSideForm";
            this.Text = "POSCustomerSideForm";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._meals.ResumeLayout(false);
            this._tableLayoutPanel2.ResumeLayout(false);
            this._tableLayoutPanel4.ResumeLayout(false);
            this._tableLayoutPanel4.PerformLayout();
            this._tableLayoutPanel5.ResumeLayout(false);
            this._tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._orderDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.GroupBox _meals;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel4;
        private System.Windows.Forms.Button _addButton;
        private System.Windows.Forms.Button _nextPageButton;
        private System.Windows.Forms.Button _previousPageButton;
        private System.Windows.Forms.Label _pageLabel;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel5;
        private System.Windows.Forms.DataGridView _orderDataGridView;
        private System.Windows.Forms.Label _totalLabel;
        private System.Windows.Forms.RichTextBox _mealDescription;
        private System.Windows.Forms.DataGridViewButtonColumn _orderDelete;
        private System.Windows.Forms.TabControl _mealTabControl;
    }
}