﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class Meal : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public Meal(string name, int price, string mealImage, string description, string category)
        {
            _name = name;
            _price = price;
            _imageName = mealImage;
            _description = description;
            _category = new Category(category);
        }

        // Get the name of this meal
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }

        // Get the price of this stuff
        public int Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                NotifyPropertyChanged(nameof(Price));
            }
        }

        // Get name and price with a \n in middle
        public string GetNamePrice()
        {
            return _name + Environment.NewLine + _price.ToString();
        }

        // Get the image name to show on button
        public string ImageName
        {
            get
            {
                return _imageName;
            }
            set
            {
                _imageName = value;
                NotifyPropertyChanged(nameof(ImageName));
            }
        }

        // Get the description of the meal
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                NotifyPropertyChanged(nameof(Description));
            }
        }

        // Get the category of this meal
        public string Category
        {
            get
            {
                return _category.CategoryName;
            }
            set
            {
                _category.CategoryName = value;
                NotifyPropertyChanged(nameof(Category));
            }
        }

        // If any thing changed this sends an event
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Clones a meal
        public Meal GetClone()
        {
            return new Meal(_name, _price, _imageName, _description, _category.CategoryName);
        }

        // override Equals
        public override bool Equals(object obj)
        {
            Meal meal = obj as Meal;
            return _name == meal.Name;
        }

        private string _name;
        private int _price;
        private string _imageName = "";
        private string _description = "";
        private Category _category;
    }
}
