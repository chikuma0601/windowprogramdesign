﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class Order
    {
        // Add a meal in order
        public void AddOrder(Meal order)
        {
            SingleOrder singleOrder = FindExist(order);
            if (singleOrder == null)
            {
                singleOrder = new SingleOrder(order);
                _order.Add(singleOrder);
            }
            else
            {
                singleOrder.Quantity += 1;
            }
            UpdateTotalPrice();
        }

        // Find if the order already exists in BindingList, if none return null
        // Note that the meal is distinguished by meal name
        private SingleOrder FindExist(Meal order)
        {
            foreach (SingleOrder orderSearched in _order)
            {
                if (orderSearched.Name == order.Name)
                    return orderSearched;
            }
            return null;
        }

        // Update the total price by summing the price again
        public void UpdateTotalPrice()
        {
            _totalPrice = 0;
            foreach (SingleOrder order in _order)
            {
                _totalPrice += order.Subtotal;
            }
        }

        // Get the total price of this order
        public int GetTotalPrice()
        {
            UpdateTotalPrice();
            return _totalPrice;
        }

        // If order list is empty
        public bool IsEmpty()
        {
            if (_order.Count == 0)
                return true;
            else
                return false;
        }

        // returns the last order added to the order list
        public SingleOrder GetLastOrder()
        {
            return _order[_order.Count - 1];
        }

        // get the last order added to the order list, but in string list form
        public List<string> GetLastOrderNamePrice()
        {
            List<string> lastOrder = new List<String>();
            lastOrder.Add(_order[_order.Count - 1].Name);
            lastOrder.Add(_order[_order.Count - 1].Subtotal.ToString());
            return lastOrder;
        }

        // Delete meal in order by index
        public void DeleteMealByIndex(int index)
        {
            _totalPrice -= _order[index].Subtotal;
            _order.RemoveAt(index);
        }

        public BindingList<SingleOrder> Orders
        {
            get
            {
                return _order;
            }
        }

        // ***************TESTING****************
        public BindingList<int> Quantity
        {
            get
            {
                BindingList<int> quantity = new BindingList<int>();
                foreach (SingleOrder order in _order)
                    quantity.Add(order.Quantity);
                return quantity;
            }
        }

        private BindingList<SingleOrder> _order = new BindingList<SingleOrder>();
        private int _totalPrice = 0;
    }
}
