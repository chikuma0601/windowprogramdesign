﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class MealSaveEventArgs : EventArgs
    {
        public string[] Meal
        {
            get;
            set;
        }

        public MealSaveEventArgs(string[] meal)
        {
            Meal = meal;
        }
    }
}
