﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    class RestaurantObserver
    {
        private POSRestaurantSideForm _restaurantForm;
        private POSCustomerSideForm _customerForm;

        public RestaurantObserver(POSRestaurantSideForm restaurantSide, POSCustomerSideForm customerSide)
        {
            _restaurantForm = restaurantSide;
            _customerForm = customerSide;
        }

        // Observes restaurant
        private void AddObservation()
        {
            _restaurantForm._mealSaveEvent += new POSRestaurantSideForm.EventHandler(NotifyCustomerSide);
        }

        // Notifies Customer Side that a meal is saved
        private void NotifyCustomerSide(object sender, MealSaveEventArgs e)
        {
            _customerForm.UpdateMealInformation(e.Meal);
        }
    }
}
