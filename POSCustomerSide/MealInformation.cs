﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace POSCustomerSide
{
    /*
     * MealInformation.cs
     * 
     * Reads information about the meals from file "mealInformation.csv"
     */
    public class MealInformation
    {
        private readonly string _mealCsvPath = "\\Resources\\mealInformation.csv";
        private readonly string _categoryCsvPath = "\\Resources\\category.csv";
        private readonly string _testPath = "\\..\\POSCustomerSide";
        private List<string> _name = new List<string>();
        private List<int> _price = new List<int>();
        private List<string> _imageName = new List<string>();
        private List<string> _description = new List<string>();
        private List<string> _category = new List<string>();
        private List<string> _menuCategory = new List<string>();
        private readonly char _splitChar = ',';
        private readonly int _nameIndex = 0;
        private readonly int _priceIndex = 1;
        private readonly int _imageIndex = 2;
        private readonly int _descriptionIndex = 3;
        private readonly int _categoryIndex = 4;

        public MealInformation(bool isTest = false)
        {
            InitializeInformation(isTest);
        }

        // get meal name
        public string[] GetMealName
        {
            get
            {
                return _name.ToArray();
            }
        }

        // Get price
        public int[] GetPrice
        {
            get
            {
                return _price.ToArray();
            }
        }

        // Get image name
        public string[] GetImageName
        {
            get
            {
                return _imageName.ToArray();
            }
        }

        // Get Description
        public string[] GetDescription
        {
            get
            {
                return _description.ToArray();
            }
        }

        public string[] GetCategory
        {
            get
            {
                return _category.ToArray();
            }
        }

        public List<string> MenuCategory
        {
            get
            {
                return _menuCategory;
            }
        }

        // Initialize the information of the meals
        private void InitializeInformation(bool isTest = false)
        {
            string absoluteMealPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + _mealCsvPath;
            string absoluteCategoryPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + _categoryCsvPath;
            if (isTest)
            {
                absoluteMealPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + _testPath + _mealCsvPath;
                absoluteCategoryPath = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())) + _testPath + _categoryCsvPath;
            }

            StreamReader mealReader = new StreamReader(File.OpenRead(@absoluteMealPath));
            StreamReader categoryReader = new StreamReader(File.OpenRead(@absoluteCategoryPath));
            ReadInformation(mealReader);
            ReadCategoryInformation(categoryReader);
        }

        // reads the informaton in file
        private void ReadInformation(StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (!String.IsNullOrWhiteSpace(line))
                {
                    string[] values = line.Split(_splitChar);
                    _name.Add(values[_nameIndex]);
                    _price.Add(Int32.Parse(values[_priceIndex]));
                    _imageName.Add(values[_imageIndex]);
                    _description.Add(values[_descriptionIndex]);
                    _category.Add(values[_categoryIndex]);
                }
            }
        }

        // reads the categories in file
        private void ReadCategoryInformation(StreamReader reader)
        {
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (!String.IsNullOrWhiteSpace(line))
                {
                    _menuCategory.Add(line);
                }
            }
        }
    }
}
