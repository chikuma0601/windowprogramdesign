﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POSCustomerSide
{
    public partial class StartUpForm : Form
    {
        private POSCustomerSideForm _customerSide;
        private POSRestaurantSideForm _restaurantSide;
        private StartUpPresentationModel _startUpPresentationModel = new StartUpPresentationModel();
        private POSCustomerSideModel _posModel = new POSCustomerSideModel();
        private RestaurantObserver _observer;

        public StartUpForm()
        {
            InitializeComponent();
        }

        // Exit the program
        private void ExitProgram(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Clicked customer side button
        private void ClickCustomerProgram(object sender, EventArgs e)
        {
            _customerSide = new POSCustomerSideForm(_posModel);
            _customerSide.FormClosed += new FormClosedEventHandler(CloseCustomerSideEvent);
            _customerSide.Show();
            _startUpPresentationModel.SetCustomerEnable(false);
            UpdateButtonEnable();
            _observer = new RestaurantObserver(_restaurantSide, _customerSide);
        }

        // Clicked restaurant side
        private void ClickRestaurantProgram(object sender, EventArgs e)
        {
            _restaurantSide = new POSRestaurantSideForm(_posModel);
            _restaurantSide.FormClosed += new FormClosedEventHandler(CloseRestaurantSideEvent);
            _restaurantSide.Show();
            _startUpPresentationModel.SetRestaurantEnable(false);
            UpdateButtonEnable();
            _observer = new RestaurantObserver(_restaurantSide, _customerSide);
        }

        // close customer side
        public void CloseCustomerSide()
        {
            _startUpPresentationModel.SetCustomerEnable(true);
            UpdateButtonEnable();
        }

        // close customer side
        public void CloseRestaurantSide()
        {
            _startUpPresentationModel.SetRestaurantEnable(true);
            UpdateButtonEnable();
        }

        // update the button enabled
        public void UpdateButtonEnable()
        {
            _startCustomerProgramButton.Enabled = _startUpPresentationModel.IsCustomerButtonEnable();
            _startRestaurantProgramButton.Enabled = _startUpPresentationModel.IsRestaurantButtonEnable();
        }

        // when customer side is closed
        public void CloseCustomerSideEvent(object sender, FormClosedEventArgs e)
        {
            CloseCustomerSide();
        }

        // when restaurant side is closed
        public void CloseRestaurantSideEvent(object sender, FormClosedEventArgs e)
        {
            CloseRestaurantSide();
        }
    }
}
