﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    public class Category
    {
        private string _category = "";
        public Category(string category)
        {
            _category = category;
        }

        public string CategoryName
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }
    }
}
