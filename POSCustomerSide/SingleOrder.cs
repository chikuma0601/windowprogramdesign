﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POSCustomerSide
{
    /*
     * SingleOrder
     * The single meal "Add" to order, also used in DataGridView
     */
    public class SingleOrder : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private Meal _meal;
        private int _quantity = 1;
        private int _subtotal = 0;

        // Contructor
        public SingleOrder(Meal meal)
        {
            _meal = meal;
            _subtotal = meal.Price;
        }

        public string Name
        {
            get
            {
                return _meal.Name;
            }
            set
            {
                _meal.Name = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }

        public string Category
        {
            get
            {
                return _meal.Category;
            }
            set
            {
                _meal.Category = value;
                NotifyPropertyChanged(nameof(Category));
            }
        }
        
        public int UnitPrice
        {
            get
            {
                return _meal.Price;
            }
            set
            {
                _meal.Price = value;
                _subtotal = _meal.Price * _quantity;
                NotifyPropertyChanged(nameof(UnitPrice));
                NotifyPropertyChanged(nameof(Subtotal));
            }
        }

        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
                _subtotal = _meal.Price * _quantity;
                NotifyPropertyChanged(nameof(Quantity));
                NotifyPropertyChanged(nameof(Subtotal));
            }
        }

        public int Subtotal
        {
            get
            {
                return _subtotal;
            }
        }

        // If any thing changed this sends an event
        // Note: Except the quantiy thing, other notifies might be useless and need to be deleted in the future
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
