﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataGridViewNumericUpDownElements;

namespace POSCustomerSide
{
    public partial class POSCustomerSideForm : Form
    {
        private CustomerPresentationModel _customerPresentationModel;
        private MealButtonUserControl _currentUserControlMenu; // currentUserControlMenu
        private List<Button> _mealButtons = new List<Button>();
        private readonly int _maxButtonOnPage = 9;
        private readonly string _mealButtonName = "_mealButton";

        public POSCustomerSideForm(POSCustomerSideModel posModel)
        {
            _customerPresentationModel = new CustomerPresentationModel(posModel);
            InitializeComponent();
            InitializeTabPages();
            SetUpCurrentUserControl();
            InitializeCurrentButton();
            UpdatePageLabel();
            SetPageButtonEnable();
            UpdateTotalPrice();
            InitializeOrderDataGridView();
            SetAddButtonEnable();
            AddDataBinding();
        }

        // Initialize the buttons with the meals recorded in Model of the current tabPage
        private void InitializeCurrentButton()
        {
            GetButtonOfUserControlMenu();
            _customerPresentationModel.InitializeButtonMap();
            SetButtonText();
            SetButtonImage();
        }

        // Setup the current user control according to current tab
        private void SetUpCurrentUserControl()
        {
            int currentTabIndex = _mealTabControl.SelectedIndex;
            _currentUserControlMenu = _mealTabControl.TabPages[currentTabIndex].Controls.Find("MealButtonUserControl", true).FirstOrDefault() as MealButtonUserControl;
        }

        // Initialize all TabPages by the number and data of categories
        private void InitializeTabPages()
        {
            BindingList<string> category = _customerPresentationModel.Category;
            for (int i = 0; i < category.Count; ++i)
            {
                _mealTabControl.Controls.Add(new TabPage());
                MealButtonUserControl userControl = new MealButtonUserControl();
                _mealTabControl.TabPages[i].Text = category[i];
                SetUpMealButtonUserControl(userControl);
                _mealTabControl.TabPages[i].Controls.Add(userControl);
            }
        }

        // sets up the Click, Dock of a MealButtonUserControl
        private void SetUpMealButtonUserControl(MealButtonUserControl userControl)
        {
            userControl.Dock = DockStyle.Fill;
            SetUpMealButtonClickEvent(GetButtonFromUserControl(userControl));
        }

        // Get the buttons on the userControl menu
        private void GetButtonOfUserControlMenu()
        {
            _mealButtons = new List<Button>();
            for (int i = 0; i < 9; ++i)
                _mealButtons.Add(_currentUserControlMenu.Controls.Find("_mealButton" + (i + 1).ToString(), true).FirstOrDefault() as Button);
        }

        // gets the buttonList from MealButtonUserControl, used at initializing tab button at startup
        private List<Button> GetButtonFromUserControl(MealButtonUserControl userControl)
        {
            List<Button> buttonList = new List<Button>();
            for (int i = 0; i < 9; ++i)
                buttonList.Add(userControl.Controls.Find(_mealButtonName + (i + 1).ToString(), true).FirstOrDefault() as Button);
            return buttonList;
        }

        // adds Click Event to buttons of MealButtonUserControl
        private void SetUpMealButtonClickEvent(List<Button> mealButtons)
        {
            mealButtons[0].Click += ClickMealButton1;
            mealButtons[1].Click += ClickMealButton2;
            mealButtons[2].Click += ClickMealButton3;
            mealButtons[3].Click += ClickMealButton4;
            mealButtons[4].Click += ClickMealButton5;
            mealButtons[5].Click += ClickMealButton6;
            mealButtons[6].Click += ClickMealButton7;
            mealButtons[7].Click += ClickMealButton8;
            mealButtons[8].Click += ClickMealButton9;
        }

        // Set the background image of the meal buttons
        private void SetButtonImage()
        {
            List<string> imagePath = _customerPresentationModel.GetMealImageAbsolutePath();
            for (int i = 0; i < imagePath.Count; ++i)
            {
                _mealButtons[i].BackgroundImage = Image.FromFile(imagePath[i]);
                _mealButtons[i].BackgroundImageLayout = ImageLayout.Stretch;
            }
        }

        // Adds dataBinding
        private void AddDataBinding()
        {
            _addButton.DataBindings.Add(nameof(_addButton.Enabled), _customerPresentationModel, nameof(_customerPresentationModel.IsAddButtonEnable));
            _nextPageButton.DataBindings.Add(nameof(_nextPageButton.Enabled), _customerPresentationModel, nameof(_customerPresentationModel.IsNextPageEnable));
            _previousPageButton.DataBindings.Add(nameof(_previousPageButton.Enabled), _customerPresentationModel, nameof(_customerPresentationModel.IsPreviousPageEnable));
            AddDataGridBinding();
        }

        // Adds binding to DataGridView
        private void AddDataGridBinding()
        {
            _orderDataGridView.DataSource = _customerPresentationModel.SingleOrders;
            DataGridViewNumericUpDownColumn numericUpDown = new DataGridViewNumericUpDownColumn();
            SetDataGridReadOnly();
            numericUpDown.DataPropertyName = nameof(Quantity);
            _orderDataGridView.Columns[nameof(Quantity)].CellTemplate = numericUpDown.CellTemplate;
        }

        // if the value of cell has changed
        private void ChangeNumericCellValue(object sender, EventArgs e)
        {

        }

        // Set all dataGridViewColumn readonly to true
        private void SetDataGridReadOnly()
        {
            foreach (DataGridViewColumn column in _orderDataGridView.Columns)
            {
                if (column.Name != nameof(Quantity))
                    column.ReadOnly = true;
            }
        }

        private BindingList<int> Quantity
        {
            get
            {
                return _customerPresentationModel.Quantity;
            }
        }

        // Adds DataBinding to NumericUpDownColumn
        private void AddNumericUpDownBinding()
        {
        }

        // Update the page label, calls method in presentation model to contact model
        private void UpdatePageLabel()
        {
            _pageLabel.Text = _customerPresentationModel.GetPageLabel;
        }

        // Add button clicked, adds meal to order
        private void AddButtonClicked(object sender, EventArgs e)
        {
            _customerPresentationModel.AddMealToOrder();
            UpdateTotalPrice();
        }

        // Initialize the dataGridView, disables user manual row resize
        private void InitializeOrderDataGridView()
        {
            _orderDataGridView.AllowUserToResizeRows = false;
            AddOrderGridViewClickEvent();
        }

        // Adds a DataGridView.CellContentCilck event to dataGridView
        private void AddOrderGridViewClickEvent()
        {
            _orderDataGridView.CellClick += new DataGridViewCellEventHandler(ClickOrderDeleteButton);
        }

        // Adds currency symbol to the end of the price /*******************DUPLICATED IN CLASS CUSTOMER_FORM.CS*****************/
        private string AddCurrencySymbol(string price)
        {
            return price + "$";
        }

        // update buttons if next page is clicked
        private void ClickNextPage(object sender, EventArgs e)
        {
            _customerPresentationModel.FlipNextPage();
            FlipPageUpdate();
        }

        // Update buttons if prev page is clicked
        private void ClickPreviousButton(object sender, EventArgs e)
        {
            _customerPresentationModel.FlipPreviousPage();
            FlipPageUpdate();
        }

        // Update appearence when Next/Previous button is clicked
        private void FlipPageUpdate()
        {
            UpdateButtonPage();
            SetPageButtonEnable();
            AddButtonUpdate();
            SetDescription();
        }

        // Update add button enable and reset selected meal
        private void AddButtonUpdate()
        {
            _customerPresentationModel.ResetSelectedMeal();
            _customerPresentationModel.RefreshAddButtonEnable();
        }
        
        // Set add button enable
        private void SetAddButtonEnable()
        {
            _addButton.Enabled = _customerPresentationModel.IsAddButtonEnable;
        }
        
        // Set next/prev page button avaliabity
        private void SetPageButtonEnable()
        {
            _customerPresentationModel.SetPageButtonEnable();
        }

        // Update meal buttons and page label
        private void UpdateButtonPage()
        {
            SetButtonText();
            UpdatePageLabel();
            SetButtonImage();
        }

        // Update the total price if order is added
        private void UpdateTotalPrice()
        {
            _totalLabel.Text = _customerPresentationModel.GetPriceText();
        }

        // Update the Text on the meal button according to the button-meal map in model
        private void SetButtonText()
        {
            List<string> namePrice = _customerPresentationModel.GetButtonText;
            for (int i = 0; i < _maxButtonOnPage; ++i)
            {
                if (i < namePrice.Count)
                    _mealButtons[i].Text = AddCurrencySymbol(namePrice[i]);
            }
            ChangeButtonVisible();
        }

        // Hide or show the unsued buttons
        private void ChangeButtonVisible()
        {
            List<bool> buttonVisible = _customerPresentationModel.GetMealButtonVisible();
            for (int i = 0; i < _mealButtons.Count; ++i)
            {
                _mealButtons[i].Visible = buttonVisible[i];
            }
        }

        // orderDelete buttonClicked and only if order delete button is clicked then delete the order selected
        private void ClickOrderDeleteButton(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                DeleteOrder(e.RowIndex);
            }
        }

        // Delete order if Delete Button is clicked
        private void DeleteOrder(int orderIndex)
        {
            if (orderIndex != -1)
            {
                _customerPresentationModel.DeleteOrder(orderIndex);
                UpdateTotalPrice();
            }
        }

        // when meal button is clicked
        private void MealButtonClicked(int mealIndex)
        {
            _customerPresentationModel.SetSelectedMeal(mealIndex);
            //SetAddButtonEnable();
            SetDescription();
        }

        // Set the description text
        private void SetDescription()
        {
            _mealDescription.Text = _customerPresentationModel.GetDescription();
        }

        // Updates meal information if is changed by mealManager
        public void UpdateMealInformation(string[] meal)
        {
            _customerPresentationModel.UpdateMealInformation(/*??????????*/);
        }

        // meal 1 button clicked
        private void ClickMealButton1(object sender, EventArgs e)
        {
            MealButtonClicked(1);
        }

        // meal 2 button clicked
        private void ClickMealButton2(object sender, EventArgs e)
        {
            MealButtonClicked(2);
        }

        // meal 3 button clicked
        private void ClickMealButton3(object sender, EventArgs e)
        {
            MealButtonClicked(3);
        }

        // meal 4 button clicked
        private void ClickMealButton4(object sender, EventArgs e)
        {
            MealButtonClicked(4);
        }

        // meal 5 button clicked
        private void ClickMealButton5(object sender, EventArgs e)
        {
            MealButtonClicked(5);
        }

        // meal 6 button clicked
        private void ClickMealButton6(object sender, EventArgs e)
        {
            MealButtonClicked(6);
        }

        // meal 7 button clicked
        private void ClickMealButton7(object sender, EventArgs e)
        {
            MealButtonClicked(7);
        }

        // meal 8 button clicked
        private void ClickMealButton8(object sender, EventArgs e)
        {
            MealButtonClicked(8);
        }

        // meal 9 button clicked
        private void ClickMealButton9(object sender, EventArgs e)
        {
            MealButtonClicked(9);
        }

        // Changed the selected tab, update menu
        private void ChangeSelectedIndex(object sender, TabControlEventArgs e)
        {
            _customerPresentationModel.ChangeTabIndex(_mealTabControl.SelectedIndex);
            InitializeCurrentButton();
            SetUpCurrentUserControl();
            GetButtonOfUserControlMenu();
            ChangeButtonVisible();
            _customerPresentationModel.ResetPage();
            FlipPageUpdate();
        }
    }
}
