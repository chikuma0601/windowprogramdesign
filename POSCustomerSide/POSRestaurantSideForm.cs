﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POSCustomerSide
{
    public partial class POSRestaurantSideForm : Form
    {
        public delegate void EventHandler(object sender, MealSaveEventArgs e);
        public event EventHandler _mealSaveEvent;

        private RestaurantPresentationModel _restaurantPresentationModel;

        public POSRestaurantSideForm(POSCustomerSideModel posModel)
        {
            InitializeComponent();
            _restaurantPresentationModel = new RestaurantPresentationModel(posModel);
            SetUpListBoxBindingList();
        }

        // sets up meal/category list box dataBinding
        private void SetUpListBoxBindingList()
        {
            SetUpMealBindingList();
            SetUpCategoryBindingList();
        }

        // Adds meal bindingList to mealListBox
        private void SetUpMealBindingList()
        {
            _mealListBox.DataSource = _restaurantPresentationModel.Meals;
            _mealListBox.DisplayMember = "Name";
        }

        // Adds category bindingList to categoryListBox
        private void SetUpCategoryBindingList()
        {
            _categoryListBox.DataSource = _restaurantPresentationModel.Categories;
            _categoryListBox.DisplayMember = "Name";
        }

        // Clicked Save button
        private void ClickSaveButton(object sender, EventArgs e)
        {
            _restaurantPresentationModel.SaveMeal();
            ThrowSaveEvent();
        }

        // Use data binding to bind all the informations displayed
        public void BindMealInformation()
        {
            _mealNameTextBox.DataBindings.Add(nameof(TextBox.Text), _restaurantPresentationModel, nameof(_restaurantPresentationModel.MealName));
            _mealPriceTextBox.DataBindings.Add(nameof(TextBox.Text), _restaurantPresentationModel, nameof(_restaurantPresentationModel.MealPrice));
            _mealImagePathTextBox.DataBindings.Add(nameof(TextBox.Text), _restaurantPresentationModel, nameof(_restaurantPresentationModel.MealImagePath));
            _mealDescriptionTextBox.DataBindings.Add(nameof(TextBox.Text), _restaurantPresentationModel, nameof(_restaurantPresentationModel.MealDescription));
            _categoryComboBox.DataSource = _restaurantPresentationModel.Categories;
            _categoryComboBox.DataBindings.Add(nameof(ComboBox.SelectedIndex), _restaurantPresentationModel, nameof(_restaurantPresentationModel.MealSelectedCategoryIndex));
            _mealSaveButton.DataBindings.Add(nameof(Button.Enabled), _restaurantPresentationModel, nameof(_restaurantPresentationModel.SaveMealButtonEnable));
        }

        // Use data binding to bind all the information displayed
        public void BindCategoryInformation()
        {
            _categoryNameTextBox.DataBindings.Add(nameof(TextBox.Text), _restaurantPresentationModel, nameof(_restaurantPresentationModel.CategoryName));
        }

        // Clear databBindings for adding new bindings
        public void ClearMealBindings()
        {
            _mealNameTextBox.DataBindings.Clear();
            _mealPriceTextBox.DataBindings.Clear();
            _mealImagePathTextBox.DataBindings.Clear();
            _mealDescriptionTextBox.DataBindings.Clear();
            _categoryComboBox.DataBindings.Clear();
            _mealSaveButton.DataBindings.Clear();
        }

        // Clear dataBindigns for add new bindings
        public void ClearCategoryBindings()
        {
            _categoryNameTextBox.DataBindings.Clear();
        }

        // Meal in MealListBox is selected
        private void SelectMeal(object sender, EventArgs e)
        {
            ClearMealBindings();
            _restaurantPresentationModel.SetSelectedMeal(_mealListBox.SelectedIndex);
            _restaurantPresentationModel.SetInformationToSelected();
            BindMealInformation();
            _restaurantPresentationModel.SaveMealButtonEnable = true;
        }

        // Category in ListBox is selected
        private void SelectCategory(object sender, EventArgs e)
        {
            ClearCategoryBindings();
            _restaurantPresentationModel.SetSelectedCategory(_categoryListBox.SelectedIndex);
            _restaurantPresentationModel.SetCategoryInformationToSelected();
            BindCategoryInformation();
        }

        // Throws a saveEvent to observer
        public void ThrowSaveEvent()
        {
            if (_mealSaveEvent != null)
            {
                _mealSaveEvent(this, new MealSaveEventArgs(_restaurantPresentationModel.SelectedMeal));
            }
        }

        // If any of the information of meal is changed
        private void ChangeInformation(object sender, EventArgs e)
        {
            _restaurantPresentationModel.SaveMealButtonEnable = true;
        }
    }
}